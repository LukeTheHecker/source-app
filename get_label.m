function [label] = get_label(coords,atlas)
addpath(genpath('/Applications/eeglab14_1_2b/plugins/fieldtrip-20181206'))

idxs = inv(atlas.transform)*[coords(1), coords(2), coords(3), 1]';
i = round(idxs(1));
j = round(idxs(2));
k = round(idxs(3));
try
    if atlas.tissue(i,j,k) ~= 0 && atlas.tissue(i,j,k) <= 116
        label = atlas.tissuelabel{atlas.tissue(i,j,k)};
        return
    end

n = 0;
while n<=5
    n = n+1;
    if  atlas.tissue(i+n,j,k) ~= 0 && atlas.tissue(i+n,j,k) <= 116
        label = atlas.tissuelabel{atlas.tissue(i+n,j,k)};
        return
    elseif atlas.tissue(i-n,j,k) ~= 0 && atlas.tissue(i-n,j,k) <= 116
        label = atlas.tissuelabel{atlas.tissue(i-n,j,k)};
        return
    elseif atlas.tissue(i,j+n,k) ~= 0 && atlas.tissue(i,j+n,k) <= 116
        label = atlas.tissuelabel{atlas.tissue(i,j+n,k)};
        return
    elseif atlas.tissue(i,j-n,k) ~= 0 && atlas.tissue(i,j-n,k) <= 116
        label = atlas.tissuelabel{atlas.tissue(i,j-n,k)};
        return
    elseif atlas.tissue(i,j,k+n) ~= 0 && atlas.tissue(i,j,k+n) <= 116
        label = atlas.tissuelabel{atlas.tissue(i,j,k+n)};
        return
    elseif atlas.tissue(i,j,k-n) ~= 0 && atlas.tissue(i,j,k-n) <= 116
        label = atlas.tissuelabel{atlas.tissue(i,j,k-n)};
        return
    end
end
catch
    label = 'outside';
end

label = 'outside';
end
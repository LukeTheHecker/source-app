function sourceinfo(app,source)
% This function extracts maxima from statistical image or Inverse Solution
% output that are volume-based.

load('aal.mat')
if length(source)>1
    error('Source Info for multiple sources not yet implemented. You could calculate an average first.')
end


if isfield(source,'stat') && isfield(source,'prob')
    myText = 'Source Statistics';
    % find maxima
    myText = [myText newline 'Stat. Significant Maxima (highest to lowest T):'];
    m = find_source_maxima(source,'stat');
    % delete statistically insignificant ones
    delete = zeros(1,size(m,1));
    for i = 1:size(m,1)
        [A,B,C] = intersect(source.pos,m(i,:),'rows');
        if isempty(B)
            [~,B]=min(sum(abs(source.pos-m(i,:)),2));
        end
        if ~(source.mask(B) == 1 && source.inside(B) == 1)
            delete(i) = 1;
        end
        strength(i) = source.stat(B);
    end
    m(find(delete==1),:) = [];
    strength(find(delete==1)) = [];
    
    
    % Sort in ascending order
    [~,idx] = sort(strength,'descend');
    m = m(idx,:);
    strength = strength(idx);
    
    for i = 1:size(m,1)
        label{i} = get_label(m(i,:), atlas);
        ispresent = numel(find(ismember(label,label{i})==1));
        if ispresent <= 1 % && ~strcmp(label{i},'outside')
            myText = [myText newline num2str(m(i,:)) sprintf('\t') label{i} sprintf('\t') num2str(round(strength(i),2))];
        end
    end
else
    myText = 'Source Activation';
    % find maxima
    myText = [myText newline 'Maxima:'];
    m = find_source_maxima(source,'avg.pow');
    % Get strength of the maxima
    for i = 1:size(m,1)
        [A,B,C] = intersect(source.pos,m(i,:),'rows');
        strength(i) = source.avg.pow(B);
    end
    % Sort in ascending order
    [~,idx] = sort(strength,'descend');
    m = m(idx,:);

    for i = 1:size(m,1)
        label{i} = get_label(m(i,:), atlas);
        myText = [myText newline num2str(m(i,:)) '    ' label{i}];
    end
end


    
app.TextArea.Value = myText;
end
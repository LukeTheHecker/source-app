function maxima = source_timecourse_mesh(app,sources_S,sources_A,sources_B)
warning off
%% find Maxima

load('aal.mat')
pos = sources_S.pos;
if length(sources_A)>1 || length(sources_B)>1 || length(sources_S)>1
    error('Source Info for multiple sources not yet implemented. You could calculate an average first.')
end

alphacrit = app.AlphacritEditField_3.Value/100;
iter = app.kMeansiterationsEditField_2.Value;
absolute = app.AbsoluteTimeCoursesCheckBox.Value;

if app.PositiveButton_3.Value == 1
    pv = true;
else
    pv = false;
end

if app.NegativeButton_3.Value == 1
    nv = true;
else
    nv = false;
end


sourcetype = 'stat';
fun = 'stat';

maxvox = max(sources_S.stat);
sparam = sources_S.stat;
p = sources_S.prob;    

% t_thresh = maxvox*percthresh;
% sparam( find(abs(sparam) < (t_thresh)) ) = NaN;
sparam(find( p >= alphacrit )) = NaN;

if pv
    sparam(find(sparam<0)) = NaN;
elseif nv
    sparam(find(sparam>0)) = NaN;
end

vox_idx = find(~isnan(sparam));

vox_pos = pos(vox_idx,:);


% Cluster the top 30% value voxels 
% [IDX,C] = kmeans_opt(vox_pos,16,0.85,100);
% left
disp('Clustering Voxels in Left Hemisphere...')
vox_pos_L = vox_pos;
vox_pos_L(find(vox_pos(:,1)<0),:) = NaN;

[IDX_L,C_L] = kmeans_opt(vox_pos_L,8,0.95,iter);
% right
disp('Clustering Voxels in Right Hemisphere...')
vox_pos_R = vox_pos;
vox_pos_R(find(vox_pos(:,1)>0),:) = NaN;
[IDX_R,C_R] = kmeans_opt(vox_pos_R,8,0.95,iter);

% Find the maximum in obtained clusters

vox_vals = sparam(vox_idx);
if isfield(sources_S, 'prob')
    vox_p = sources_S.prob(vox_idx);
else
    vox_p = ones(size(vox_vals));
end


% Left
for i = 1:size(C_L,1)
    tmp = NaN(size(vox_idx));
    tmp(find(IDX_L==i)) = vox_p(find(IDX_L==i));
    [val_l(i),maxidx_l(i)] = min(abs(tmp));
    label_l{i} = get_label(vox_pos(maxidx_l(i),:),atlas);
    label_l{i} = strrep(label_l{i},'_',' ');
end
% Right
for i = 1:size(C_R,1)
    tmp = NaN(size(vox_idx));
    tmp(find(IDX_R==i)) = vox_p(find(IDX_R==i));
    [val_r(i),maxidx_r(i)] = min(abs(tmp));
    label_r{i} = get_label(vox_pos(maxidx_r(i),:),atlas);
    label_r{i} = strrep(label_r{i},'_',' ');
end

C = [C_L;C_R];
label = [label_l,label_r];
val = [val_l,val_r];
maxidx = [maxidx_l,maxidx_r];

timecourse = NaN(2,size(C,1),size(sources_A(1).avg.pow,2));
plotdim = ceil(sqrt(size(timecourse,2)));

disp('plotting...')
figure
time = sources_A.time(1:end);
for i = 1:size(C,1)
    [~,idx]= min(sum(abs(sources_A.pos - vox_pos(maxidx(i),:)),2));
    if absolute
        timecourse(1,i,:) = abs(sources_A.avg.pow(idx,:));
        timecourse(2,i,:) = abs(sources_B.avg.pow(idx,:));
    else
        timecourse(1,i,:) = sources_A.avg.pow(idx,:);
        timecourse(2,i,:) = sources_B.avg.pow(idx,:);
    end
    
    subplot(plotdim,plotdim,i)
    plot(time(1:size(sources_A(1).avg.pow,2)),squeeze(timecourse(1,i,:)))
    hold on
    plot(time(1:size(sources_A(1).avg.pow,2)),squeeze(timecourse(2,i,:)))
    hold on
    plot([min(time),max(time)],[0,0],'k')   % horizontal line
    hold on
    plot([0,0],[min([timecourse(1,i,:),timecourse(2,i,:)],[],'all')*1.2,max([timecourse(1,i,:),timecourse(2,i,:)],[],'all')*1.2],'k')   % vertical line
    xlim([min(time),max(time)])
    ylim([min([timecourse(1,i,:),timecourse(2,i,:)],[],'all')*1.2,max([timecourse(1,i,:),timecourse(2,i,:)],[],'all')*1.2])
    format long
    title(sprintf('%s, p = %.2f',label{i},val(i)))
    if i == 1
        legend('Sources A','Sources B')
    end
end


% % testing
% 
% figure
% plot(time,sources_A.avg.pow(1,:))
% hold on
% plot(time,sources_A.avg.pow(50,:))

for i = 1:size(C,1)
    maxima(i).pos = vox_pos(maxidx(i),:);
    maxima(i).label = label(i);
    maxima(i).p = val(i);
    maxima(i).t = vox_vals(maxidx(i));
    maxima(i).timecourse = squeeze(timecourse(:,i,:));
    maxima(i).time = time(1:size(timecourse,3));
end
disp('done')
warning on

end


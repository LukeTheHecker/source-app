function source_timecourse(app)

load('aal.mat')
pth_A = app.SourceAAVGEditField.Value;
pth_B = app.SourceBAVGEditField.Value;
pth_S = app.StatisticEditField.Value;

if isempty(pth_A) && isempty(pth_B)
    return
end


if isempty(pth_S) || ~exist(pth_S) == 2
    error('path does not exist or not entered')
end

sources_S = load(pth_S);
fn = fieldnames(sources_S);
sources_S = sources_S.(fn{1});
mymaxima =  find_source_maxima(sources_S,'stat');

% Check if maxima are within the aal locations
cnt = 1;
for i = 1:size(mymaxima,1)
    mylabels{i} = strrep(get_label(mymaxima(i,:),atlas),'_',' ');
    if ~strcmp(mylabels{i}, 'outside')
        mymaxima_tmp(cnt,:)  = mymaxima(i,:);
        mylabels_tmp{cnt} = mylabels{i};
        cnt = cnt+1;
    end
end
mymaxima = mymaxima_tmp;
mylabels = mylabels_tmp;
% sort maxima by T values (high to low) and remove non-significant
% entries
tvals = [];
pvals = [];
for i = 1:size(mymaxima,1)
    [~,idx]= min(sum(abs(sources_S.pos - mymaxima(i,:)),2));
    tvals(i) = sources_S.stat(idx);
    pvals(i) = sources_S.prob(idx);
end

notsig = zeros(size(pvals));
for i = 1:size(mymaxima,1)
    if pvals(i)>0.05
        notsig(i) = 1;
    end
end
mymaxima(find(notsig==1),:) = [];
mylabels(find(notsig==1)) = [];
tvals(find(notsig==1)) = [];
pvals(find(notsig==1)) = [];



tvals(isnan(tvals)) = 0;
[tvals,idxa] = sort(tvals,'descend');
mymaxima = mymaxima(idxa,:);
mylabels = mylabels(idxa);


if ~isempty(pth_A)
    sources_A = load(pth_A);
    fn = fieldnames(sources_A);
    sources_A = sources_A.(fn{1});
end

if ~isempty(pth_B)
    sources_B = load(pth_B);
    fn = fieldnames(sources_B);
    sources_B = sources_B.(fn{1});
end


timecourse = NaN(2,size(mymaxima,1),numel(sources_A(1).time));
plotdim = ceil(sqrt(size(timecourse,2)));
figure
for i = 1:size(mymaxima,1)
    [~,idx]= min(sum(abs(sources_A.pos - mymaxima(i,:)),2));
    timecourse(1,i,:) = sources_A.avg.pow(idx,:);
    timecourse(2,i,:) = sources_B.avg.pow(idx,:);
    
    subplot(plotdim,plotdim,i)
    plot(sources_A.time,squeeze(timecourse(1,i,:)))
    hold on
    plot(sources_B.time,squeeze(timecourse(2,i,:)))
    format long
    title(sprintf('%s T = %.2f',mylabels{i},tvals(i)))
    if i == 1
        legend('Sources A','Sources B')
    end
end


end


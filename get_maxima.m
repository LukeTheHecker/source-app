function mymaxima = get_maxima(source,fun)
disp('Finding Maxima...')
if strcmp(fun,'avg.pow')
    if size(source.avg.pow,2)>1
         tmp_source = mean(source.avg.pow(:,102:end),2);
    else
        tmp_source = source.avg.pow;
    end
elseif strcmp(fun,'avgpow')
    tmp_source = source.avgpow;
elseif strcmp(fun,'pow')
    tmp_source = mean(source.pow,2);
else
    if size(source.(fun),2)>1
        tmp_source = mean(source.(fun)(:,102:end),2);
    else
        tmp_source = source.(fun);
    end
end

tmp_source(isnan(tmp_source)) = 0;
tmp_source_3d = reshape(tmp_source, [source.dim]);

bw = imregionalmax(tmp_source_3d);

bw_ls = reshape(bw, [1,prod(source.dim)]);
bw_ls(~source.inside) = false;

mymaxima = source.pos(find(bw_ls==1),:);
end
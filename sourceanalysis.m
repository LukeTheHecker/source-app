function sourceTime = sourceanalysis(EEG,app,method)
% store useful parameters
fwd_path        = app.PathForwardSolutionEditField.Value;
lambda          = app.LambdaEditField.Value;
baseline        = [app.StartEditField.Value,app.StopEditField.Value];
timerange       = [app.StartEditField_2.Value,app.StopEditField_2.Value];

cfg_sa.method   = method;
crossval        = app.CrossValidateCheckBox.Value;

% create ERP and calculate noise
[data_tl,data,pnts] = get_timelock(EEG,timerange,baseline);
% load headmodel & leadfield
models = load(fwd_path);
leadfield = models.leadfield;
headmodel = models.headmodel;

% source localization
sourceTime=source_localization(cfg_sa,data_tl,data,pnts,headmodel,leadfield,lambda,baseline,crossval);

sourceTime = trimSource(sourceTime);
 

disp('Source Analysis Done')
end

function [data_tl,data,pnts]=get_timelock(EEG,timerange,baseline)
%% Transform eeglab struc to fieldtrip struc, calculate ERP (mean) and noise
% estimation for eloreta

EEG.times = round(EEG.times);
data = eeglab2fieldtrip(EEG, 'preprocessing', 'none');
% Timelockanalysis
cfg = [];
% Matrix to estimate noise for eLORETA
cfg.covariance = 'yes';
% Time Window to estimate noise
cfg.covariancewindow = [baseline(1) baseline(2)];
cfg.removemean = 'no';
% cfg.latency = [timerange(1) timerange(2)];
[data_tl] = ft_timelockanalysis(cfg, data);
%% Align Electrodes
try
    elec_template = ft_read_sens('standard_1020.elc');
    data_tl.elec = get_chanlocs_from_labels(data_tl.elec,elec_template);
catch
    elec_template = ft_read_sens('standard_1005.elc');
    data_tl.elec = get_chanlocs_from_labels(data_tl.elec,elec_template);
end

% Plot Training & Test data subsample
% figure
% plot(data_tl_training.time,data_tl_training.avg(14,:))
% hold on
% plot(data_tl_test.time,data_tl_test.avg(14,:))
    


% pnts
[~,lo] = min(abs(data_tl.time-timerange(1)));
[~,hi] = min(abs(data_tl.time-timerange(2)));
pnts = lo:hi;

end

function [sourceTime]=source_localization(cfg_sa,data_tl,data,pnts,headmodel,leadfield,lambda,baseline,crossval)

cfg_sa.grid        = leadfield;
cfg_sa.headmodel   = headmodel;

if strcmp(cfg_sa.method,'eloreta')
    cfg_sa.eloreta.lambda       = lambda;
    cfg_sa.latency              = [data_tl.time(min(pnts)),data_tl.time(max(pnts))];
elseif strcmp(cfg_sa.method,'sloreta')
    cfg_sa.sloreta.lambda       = lambda;
    cfg_sa.latency = [data_tl.time(min(pnts)),data_tl.time(max(pnts))];

elseif strcmp(cfg_sa.method,'mne')
    cfg_sa.mne.prewhiten       = 'yes';
    cfg_sa.mne.lambda          = 3;
    cfg_sa.mne.scalesourcecov  = 'yes';
elseif strcmp(cfg_sa.method,'lcmv')
    cfg_sa.lcmv.lambda     = [num2str(round(lambda*100)) '%'];
    cfg_sa.senstype        = 'EEG';
elseif strcmp(cfg_sa.method,'sam')
    cfg_sa.channel        = 'EEG';
elseif strcmp(cfg_sa.method,'GS')
    p = path;
    disp('call multiplesparseprios')
    method = 'GS';
    sourceTime = multiplesparsepriors_2(data_tl,method,[]);
    disp('Done with Greedy Search')
    path(p)
elseif strcmp(cfg_sa.method,'GS')
    p = path;
    disp('call multiplesparseprios')
    method = cfg_sa.method;
    sourceTime = multiplesparsepriors_2(data_tl,method);
    disp('Done with Greedy Search')
    path(p)
elseif strcmp(cfg_sa.method,'ARD')
    p = path;
    disp('call multiplesparseprios')
    method = cfg_sa.method;
    sourceTime = multiplesparsepriors_2(data_tl,method);
    disp('Done with Greedy Search')
    path(p)
elseif strcmp(cfg_sa.method,'LOR')
    p = path;
    disp('call multiplesparseprios')
    method = cfg_sa.method;
    sourceTime = multiplesparsepriors_2(data_tl,method);
    disp('Done with Greedy Search')
    path(p)
elseif strcmp(cfg_sa.method,'IID')
    p = path;
    disp('call multiplesparseprios')
    method = cfg_sa.method;
    sourceTime = multiplesparsepriors_2(data_tl,method);
    disp('Done with Greedy Search')
    path(p)
elseif strcmp(cfg_sa.method,'COH')
    p = path;
    disp('call multiplesparseprios')
    method = cfg_sa.method;
    sourceTime = multiplesparsepriors_2(data_tl,method);
    disp('Done with Greedy Search')
    path(p)
end
% approaches to set lambda:
% eigratio = min(myeigenvalues)/min(myeigenvalues)
% lambda_k = 1000/eigratio;
% max(myeigenvalues)/1000
if crossval
    cv_lambda = crossval_lambda(data, cfg_sa,baseline);
    cfg_sa.(cfg_sa.method).lambda = cv_lambda;
end
if ~exist('sourceTime')
    sourceTime = ft_sourceanalysis(cfg_sa, data_tl);

    % for convenience also compute power (over the three orientations) at each location and for each time

    pow = nan(size(sourceTime.pos,1), size(data_tl.avg,2));
    for i=1:size(sourceTime.pos,1)
        try
            pow(i,:) = sum(abs(sourceTime.avg.mom{i}).^2, 1);
        catch
        end
    end

    sourceTime.avg.pow = pow;
end



end
function source_plot_medi(EEG,source,g)
% MRI file as a structural template
mri = load('-mat', EEG.dipfit.mrifile);
mri = ft_volumereslice([], mri.mri);


cfg            = [];
cfg.downsample = 2;
cfg.parameter = 'pow';
% source.oridimord = 'pos';
% source.momdimord = 'pos';
source_interp  = ft_sourceinterpolate(cfg, source , mri);

cfg              = struct(g.ft_sourceplot_params{:});
cfg.funparameter = 'pow';
cfg.surffile       = 'surface_white_both.mat'; % Cortical sheet from canonical MNI brain

cfg.camlight = 'no'; % illumination of the brain
ft_sourceplot(cfg,source_interp);


end
function source = trimSource(source)
% trim sources_A/B by removing redundant information
for isub = 1:numel(source)
    if isfield(source(isub).avg,'mom')
        source(isub).avg.mom = [];
    end
    if isfield(source(isub).avg,'ori')
        source(isub).avg.ori = [];
    end
    if isub>1
        if isfield(source,'time')
            source(isub).time = [];
        end
        if isfield(source,'inside')
            source(isub).inside = [];
        end
        if isfield(source,'pos')
            source(isub).pos = [];
        end
        if isfield(source,'tri')
            source(isub).tri = [];
        end
        if isfield(source,'method')
            source(isub).method = [];
        end
        if isfield(source,'cfg')
            source(isub).cfg = [];
        end
    end
end

end
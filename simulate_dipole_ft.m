function dipsim = simulate_dipole_ft(app,ALLEEG)
% initial stuff
savepath C:\Users\Lukas\Documents\SourcePlugin\temp\pathdef.m


ndip = app.NumberofDipolesEditField.Value;
nsim = app.NoofSimulationsEditField.Value;
mindist = app.MindistancebetweenEditField.Value;
pth_fwdsol = app.PathForwardSolutionEditField_2.Value;
inverse_solution = app.InverseSolutionDropDown_2.Value;
SNR = app.SNREditField.Value;
lambda = app.LambdaEditField_3.Value;
mindist = app.MindistancebetweenEditField.Value;
ntrials = app.NoTrialsEditField.Value;
ica = app.ICACheckBox.Value;
% Manage Paths
s = what('fieldtrip');
s.path

pth_electemplate = [s.path, '\template\electrode\standard_1005.elc'];

switch inverse_solution
    case 'eLORETA'
        method = 'eloreta';
    case 'sLORETA'
        method = 'sloreta';
    case 'MNE'
        method = 'mne';
    case 'LCMV'
        method = 'lcmv';
    case 'SAM'
        error('Not (yet) implemented for EEG data')
    case 'MUSIC'
        method = 'music';
    case 'HARMONY'
        method = 'harmony';
end

if strcmp(app.InverseSolutionDropDown_2.Value,'Equivalent Dipole') && ndip > 1
    error('You are Simulating more than 1 dipole and use a single dipole solution.')
end    
    
% load hm
load(pth_fwdsol);
% get elec
if app.TemplateN346Button.Value
    elec_info = ft_read_sens(pth_electemplate);
    elec.label = elec_info.label;
    elec.pnt = elec_info.chanpos;
else
    try
        EEG_tmp = pop_loadset(ALLEEG(1).filename,ALLEEG(1).filepath);
        data = eeglab2fieldtrip(EEG_tmp, 'preprocessing');
        elec = data.elec;
        %% Align Electrodes
        try
            elec_template = ft_read_sens('standard_1020.elc');
            elec = get_chanlocs_from_labels(elec,elec_template);
        catch
            elec_template = ft_read_sens('standard_1005.elc');
            elec = get_chanlocs_from_labels(elec,elec_template);
        end
    catch
        error('No EEGLAB dataset or electrode positions available. Load a valid dataset or choose to use a template!')
    end
end
%% Generate Random Dipole(s):
% Signal & Noise Properties
loc_err = NaN(1,nsim);
time = (1:250)/250;
freqlim = [3,8];

if SNR~=inf
    relnoiselvl = (10^(SNR/10))^-1;     % convert to dB
else
    relnoiselvl = 0;
end

f = waitbar(0,'please wait...');
for isim = 1:nsim
 	waitbar(isim/nsim, f, sprintf('Simulation %d/%d',isim,nsim))
    
    % Simulate Dipole
    [raw,simdip_pos{isim},simdip_mom{isim},signal{isim}] = simdip(headmodel,leadfield,elec,ndip,ntrials,relnoiselvl,mindist,time,freqlim);
    
    
    % rereference to common average
    cfg = [];
    cfg.reref = 'yes';
    cfg.refchannel = 'all';
    raw = ft_preprocessing(cfg,raw);    


    cfg = [];
    cfg.covariance = 'yes';
    % Time Window to estimate noise
    cfg.covariancewindow = [0 0.4];
    avg = ft_timelockanalysis(cfg, raw);
    % Check if Inverse Solution is Equivalent Dipole or distributed
    if strcmp(inverse_solution,'Equivalent Dipole')
        % Exhaustive dipfit
        cfg = [];
        cfg.headmodel = headmodel;    % see above
        cfg.sourcemodel= leadfield;
        cfg.elec = elec;        % see above
        cfg.gridsearch = 'yes';
        dip = ft_dipolefitting(cfg, avg);
        loc_err(isim) = eucl(dip.dip.pos,simdip_pos{isim});
        fprintf('localization error = %d mm\n', round(eucl(dip.dip.pos,simdip_pos{isim}),2))
    else
        if strcmp(inverse_solution, 'eLORETA')
            if ica
                % Test: perform ICA prior to source analysis
                cfg        = [];
                cfg.method = 'runica'; % this is the default and uses the implementation from EEGLAB
                cfg.runica.pca = ndip*2; % allow twice as many dimensions as there are components for noise separation
                cfg.runica.extended = 1;
                comp = ft_componentanalysis(cfg, raw);
                % iterate through the top components since they explain
                % most variance
                for icmp = 1:ndip
                    cmpidx = 1:size(comp.topo,2);
                    rejcmps = find(cmpidx~=icmp);

                    cfg = [];
                    cfg.component = rejcmps;
                    single_comp = ft_rejectcomponent(cfg, comp,raw);

                    cfg=[];
                    cfg.covariance = 'yes';
                    % Time Window to estimate noise
                    cfg.covariancewindow = [0 0.4];
                    avg = ft_timelockanalysis(cfg, single_comp);

                    cfg = [];
                    cfg.elec = elec;
                    cfg.grid = leadfield;
                    cfg.headmodel = headmodel;
                    cfg.method = method;
                    cfg.eloreta.lambda = lambda;
                    source(icmp) = ft_sourceanalysis(cfg,avg);
                end
            else
                cfg = [];
                cfg.elec = elec;
                cfg.grid = leadfield;
                cfg.headmodel = headmodel;
                cfg.method = method;
                cfg.eloreta.lambda = lambda;
                source = ft_sourceanalysis(cfg,avg);
            end
        elseif strcmp(inverse_solution,'sLORETA')
            cfg = [];
            cfg.elec = elec;
            cfg.grid = leadfield;
            cfg.headmodel = headmodel;
            cfg.method = method;
            cfg.sloreta.lambda = lambda;
            source = ft_sourceanalysis(cfg,avg);
        elseif strcmp(inverse_solution,'MNE')
            cfg = [];
            cfg.elec = elec;
            cfg.grid = leadfield;
            cfg.headmodel = headmodel;
            cfg.method = method;
            cfg.mne.lambda = lambda;
            cfg.mne.prewhiten = 'yes';
            cfg.mne.scalesourcecov = 'yes';
            source = ft_sourceanalysis(cfg,avg);
        elseif strcmp(inverse_solution,'LCMV')
            cfg = [];
            cfg.elec = elec;
            cfg.grid = leadfield;
            cfg.headmodel = headmodel;
            cfg.method = method;
            cfg.lcmv.lambda = [num2str(round(lambda*100)) '%'];
%             cfg.lcmv.lambda = lambda;
%             cfg.lcmv.fixedori    = 'yes';
            cfg.lcmv.projectnoise = 'yes';
            cfg.senstype = 'EEG';
            source = ft_sourceanalysis(cfg,avg);
            % Neural Activity Index
%             source.avg.pow = source.avg.pow ./ source.avg.noise;
            
        elseif strcmp(inverse_solution,'MUSIC')
            avg = rmfield(avg,'cov');
            cfg=[];
            cfg.elec = elec;
            cfg.grid = leadfield;
            cfg.headmodel = headmodel;
            cfg.method = method;
            cfg.music.numcomponent = ndip;

            source = ft_sourceanalysis(cfg,avg);
            source.avg.pow = source.avg.jr.^-1;
        elseif strcmp(inverse_solution,'HARMONY')
            error('Not implemented yet. Reason: Problem with different sizes of vertices and faces in leadfield.')
            cfg=[];
            cfg.elec = elec;
            cfg.grid = leadfield;
            cfg.headmodel = headmodel;
            cfg.harmony.lambda = lambda;
            cfg.lambda = lambda;
            cfg.harmony.noisecov = avg.cov;
            cfg.harmony.number_harmonics = [];
            cfg.harmony.connected_components = 1;
%             cfg.harmony.filter_bs = 30;
%             cfg.harmony.filter_order = 100;
            cfg.method = method;

            source = ft_sourceanalysis(cfg,avg);
        end
        for ii = 1:length(source)
            % check if source.avg.pow is in right shape
            if size(source(ii).avg.pow,1) < size(source(ii).avg.pow,2)
                source(ii).avg.pow = source(ii).avg.pow';
            end
            % Calc Power for each timepoint
            if size(source(ii).avg.pow,2) == 1 && isfield(source(ii).avg,'mom')
                pow = nan(size(source(ii).pos,1), size(avg.avg,2));
                for i=1:size(source(ii).pos,1)
                    try
                        pow(i,:) = sum(abs(source(ii).avg.mom{i}).^2, 1);

                    catch
                    end
                end
                source(ii).avg.pow = pow;
            end

            % average over time
            if size(source(ii).avg.pow,2) > 1
                source(ii).avg.pow = mean(source(ii).avg.pow(:,102:end),2);
            end
        end
        % find maximum of the distributed dipole solution
            
        if ndip == 1
            source(ii).avg.pow(source(ii).inside == 0) = NaN;
            [~,idx] = max(source(ii).avg.pow);
            loc_err(isim) = eucl(source(ii).pos(idx,:),simdip_pos{isim});
        elseif ndip > 1
            % check if source was computed for independent comps
            % individually
            if length(source)>1
                for ii = 1:length(source)
                    source(ii).avg.pow(source(ii).inside == 0) = NaN;
                    [~,idx] = max(source(ii).avg.pow);
                    m(ii,:) = source(ii).pos(idx,:);
                end
            else
                % Find maxima of source.avg.pow
                m = find_source_maxima(source,'avg.pow',0.2,0,ndip,5,'pos');
            end
            % Check which maxmimum is closest to first and second dipole
            for k = 1:ndip
                for j = 1:size(m,1)
                    max_eucl(k,j) = eucl(m(j,:), simdip_pos{isim}(k,:));
                end
            end
            loc_err(isim) = mean(min(max_eucl,[],2));
            max_eucl = [];
            m = [];
        end

    end
    
end
close(f)

%% Summarize Results

% Get Dipole Depth (min distance between each simulated dipole and
% electrodes)
for i = 1:nsim
    for j = 1:ndip
        depth(i,j) = min(eucl(elec.elecpos,simdip_pos{i}(j,:)));
    end
end
% calculate mean source depth per simulation
depth = mean(depth,2)';
% calculate correlation between mean source depth and localization error
depthcorr = min(corrcoef(depth,loc_err),[],'all');

% Get average distance between dipoles of each simulation
for i = 1:nsim
    % distance between each pair of simulated dipoles
    for j = 1:ndip
        for k = 1:ndip
            distmat(j,k) = eucl(simdip_pos{i}(j,:),simdip_pos{i}(k,:));
        end
    end
    lowtri = tril(distmat,-1);
    avgdist(i) = mean(lowtri(find(lowtri~=0)));
end
distcorr = min(corrcoef(avgdist,loc_err),[],'all');

% Get average correlation between source activity in each simulation
for i = 1:nsim
    % distance between each pair of simulated dipoles
    for j = 1:ndip
        for k = 1:ndip
             signalcorr(j,k) = abs(min(corrcoef(signal{i}(j,:),signal{i}(k,:)),[],'all'));
        end
    end
    lowtri = tril(signalcorr,-1);
    avgsignalcorr(i) = mean(lowtri(find(lowtri~=0)));
end
sig_corr_loc_corr = min(corrcoef(avgsignalcorr,loc_err),[],'all');

format short g
results = sprintf('%d simulations of %d randomly set dipole(s). SNR: %d, Minimum Distance: %d mm. Inverse Solution: %s\nMean Localisation Error: %.2f mm +- %.2f mm\nCorrelation [Source Depth X Localisation Error]: %.2f \nCorrelation [Mean Source Distance X Localisation Error]: %.2f \nCorrelation [inter-source timecourse correlation X Localisation Error]: %.2f',nsim, ndip,SNR,mindist,inverse_solution,round(mean(loc_err),2),round(std(loc_err),2),round(depthcorr,2),round(distcorr,2),round(sig_corr_loc_corr,2));

app.ResultsTextArea.Value = results;

dip.correct.simdip_pos = simdip_pos;
dip.correct.simdip_mom = simdip_mom;
dip.localizationError = loc_err;
dipsim = dip;
end

function [raw,simdip_pos,simdip_mom,signal] = simdip(headmodel,leadfield,elec,ndip,ntrials,relnoiselvl,mindist,time,freqlim)
    simdip_pos = [];
    simdip_mom = [];
    
    % Generate sinusoidal signal
    for i = 1:ndip
        freq = (freqlim(2)-freqlim(1)).*rand(1)+freqlim(1); % some frequency between 3 and 8 Hz
        signal(i,:) = sin(freq*time*2*pi);
        signal(i,1:101) = 0; % Baseline
    end
    
    for idip = 1:ndip
        % take a dipole from the grid defined by the leadfield which is inside
        % the brain
        
        if idip==1
        tmp_dip = datasample(leadfield.pos(leadfield.inside==1,:),1);
        elseif idip>1
            % try to find a dipole with a distance equal to or greater than
            % mindist
            while tmp_dist < mindist
                tmp_dip = datasample(leadfield.pos(leadfield.inside==1,:),1);
                for k = 1:idip-1
                    tmp_dists(k) = eucl(tmp_dip,simdip_pos(k,:));
                end
                tmp_dist = min(tmp_dists);
                disp('re-searching')
            end
        end
        simdip_pos = [simdip_pos;tmp_dip];

        x_mom = rand(1,1);
        y_mom = rand(1,1);
        z_mom = rand(1,1);
        simdip_mom = [simdip_mom; x_mom,y_mom,z_mom];
        tmp_dist = -999999;
    end
    % Dipole Simulation With freq = 7 Hz sine wave (single dipole) and a
    % random number between 3 and 8 Hz for each extra dipole
    cfg = [];
    cfg.headmodel = headmodel;        % see above
    cfg.elec = elec;            % see above
    cfg.dip.pos = simdip_pos;
    cfg.dip.mom = simdip_mom';     % note, it should be transposed
    % repeat signal for ntrials
    for h = 1:ntrials
        signal_rep(h) = {signal};
    end
    cfg.dip.signal = signal_rep; 
    cfg.fsample = 250;          % Hz
    cfg.relnoise = relnoiselvl;
    raw = ft_dipolesimulation(cfg);
end
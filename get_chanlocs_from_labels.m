function elec = get_chanlocs_from_labels(elec,elec_template)

for i = 1:numel(elec.label)
    idx = find(strcmp(elec_template.label,elec.label{i}))
    elec.pnt(i,:) = elec_template.chanpos(idx,:)
    elec.elecpos(i,:) = elec_template.chanpos(idx,:);
    elec.chanpos(i,:) = elec_template.chanpos(idx,:);
    elec.chantype(i,:) = elec_template.chantype(idx,:);
    elec.chanunit(i,:) = elec_template.chanunit(idx,:);
    if isempty(idx) 
        error(sprintf('Did not find channel %s ! Change your electrode layout in "elec_template"',elec.label{i}))
    end

end
elec.unit = elec_template.unit;
elec.type = elec_template.type;

end


function [sources_A,sources_B,sources_A_B] = multiplesparsepriors_2(app,ALLEEG,raw,method,woi,foi)
% Multiple Sparse Priors using GS or ARD
% Based on: spm_eeg_inv_help

disp('Multiple Sparse Priors Source Analysis')

p = path;

% Manage Paths
load('defaults.mat')
restoredefaultpath
addpath(defaults.spm12)
cd(defaults.spm12)


pth_sa = app.pth_sa.path;
temppath = [pth_sa,'/temp/']; 
myfilespath = [pth_sa, '/myFiles/'];
spmfiles = [pth_sa, '/myFiles/SPM/']; 
addpath(genpath(pth_sa))
condA = app.ConditionAEditField_2.Value;
condB = app.ConditionBEditField_2.Value;
groupinversion = app.GroupInversionCheckBox.Value;
% specify cortical mesh size (1 to 3)
%--------------------------------------------------------------------------
if app.LowButton.Value
    Msize = 1;
elseif app.NormalButton.Value
    Msize = 2;
elseif app.HighButton.Value
    Msize = 3;
end



% spm eeg
spm('defaults','eeg');

for i = 1:length(raw)
    D{i}  = spm_eeg_ft2spm(raw(i),[temppath,'set_',num2str(i)]);
    plist{i} = [temppath,'set_',num2str(i),'.mat'];
    val     = 1;
    D{i}.val             = val;
    D{i}.inv{val}.method = 'Imaging';
end
% evoked or induced?
if app.evokedButton.Value
    if ~isempty(condA) && ~isempty(condB)
        for i = 1:length(D)
            D{i} = conditions(D{i}, 1, condA);
            D{i} = conditions(D{i}, 2, condB);
        end
    elseif ~isempty(condA) && isempty(condB)
        D{i} = conditions(D{i},1, condA);
    else
        D{i} = conditions(D{i},1, 'condA');
    end
elseif app.inducedButton.Value
    % Determine condition types of the trials in the spm data set D
    if ~isempty(condA) && ~isempty(condB)
        cntA = 0;
        cntB = 0;
        if ~isempty(ALLEEG)
            for i = 1:length(D)
                for k = 1:length(ALLEEG(i).event)
                    if strcmp(ALLEEG(i).event(k).type,condB)
                        cntB = cntB+1;
                    elseif strcmp(ALLEEG(i).event(k).type,condA)
                        cntA = cntA+1;
                    end
                end
                D{i} = conditions(D{i}, 1:cntA, condA);  % Sets the condition label
                D{i} = conditions(D{i}, cntA+1:numel(raw(i).trial), condB);  % Sets the condition label
                cntA = 0;
                cntB = 0;
            end
        else 
            error('Your ALLEEG structure does not contain any data sets! Please load some data in EEGLAB!')
        end
    end
end                

% Compute a head model
%==========================================================================
% The next step is to define the head model in terms of s structural MRI (sMRI) 
% This is not necessary if you are happy using a standard head model supplied 
% with SPM.  We will assume this analysis is going to use a standard model, so 
% the normalisation and mesh routines are commented out in this script
%--------------------------------------------------------------------------
 
 
% Use a template head model and associated meshes
%--------------------------------------------------------------------------
% meshfname = ['C:\Users\Lukas\Documents\sourceplugin\myFiles\SPM\mesh_',num2str(Msize),'.mat'];
meshfname = [spmfiles 'mesh_',num2str(Msize),'.mat'];

if ~(exist(meshfname)==2)
    D{1} =  spm_eeg_inv_mesh_ui(D{1}, 1, Msize, 1);
    % store it for later use
    mesh = D{1}.inv{1}.mesh;
    save(meshfname,'mesh')
else
    load(meshfname)
    D{1}.inv{1}.mesh = mesh;
end
for i = 1:length(D)
    D{i}.inv{1}.mesh = mesh;
end

% Determine the modality of the data (EEG or MEG)
%--------------------------------------------------------------------------
modality = D{1}.modality;
if ~ismember(modality, {'EEG', 'MEG', 'Multimodal'})
    error('Unsupported modality')
end

% Compute a head model
%==========================================================================
% Next, we need to register the sensor locations to the head model meshes. 
% This assumes that sensor positions and fiducials are already correctly
% specified in the dataset. This step will usually be interactive
%--------------------------------------------------------------------------
for i = 1:length(D)
    S = [];
    S.task = 'defaulteegsens';
    S.D = D{i};
    D{i} = spm_eeg_prep(S);
end

hmfname = [spmfiles 'hm_bem'];

if ~(exist(hmfname)==2)
    D{1} = spm_eeg_inv_datareg_ui(D{1});
    datareg = D{1}.inv{1}.datareg;
    save(hmfname,'datareg')
else
    load(hmfname)
    D{1}.inv{1}.datareg = datareg;
end
for i = 1:length(D)
    D{i}.inv{1}.datareg = datareg;
end

% Compute a forward model
%==========================================================================
% Next, using the geometry of the head model and the location of registered 
% sensors, we can now compute a forward model for each dipole and save it in a 
% lead-field or gain matrix.  This is the basis of our likelihood model.
%--------------------------------------------------------------------------
% Without UI:

lffname = [spmfiles 'lf_bem.mat'];

for i = 1:length(D)
    D{i}.inv{1}.forward(1).voltype = 'EEG BEM';
end

if ~(exist(lffname)==2)
    D{1} = spm_eeg_inv_forward(D{1});

    forward = D{1}.inv{1}.forward;
    save(lffname,'forward')
else
    load(lffname)
    D{1}.inv{1}.forward = forward;
end
for i = 1:length(D)
    D{i}.inv{1}.forward = forward;
end


% With UI: 
% D = spm_eeg_inv_forward_ui(D);

% Invert the forward model
%==========================================================================
% Next, we invert the forward model using the trials or conditions of interest, 
% specified in the field 'trials'.  The full model needs specifying in terms
% of its priors, through the fields below.
 
%--------------------------------------------------------------------------
% load gainmatrix if it already exists from previous inversions:
if exist([spmfiles 'spm_gainmat_mesh' num2str(Msize) '.mat']) == 2
    lf_folder = fileparts(which(['spm_gainmat_mesh' num2str(Msize) '.mat']));
    lf_fullpath = [lf_folder '/spm_gainmat_mesh' num2str(Msize) '.mat'];
%     lf = load(lf_fullpath);
    for i = 1:length(D)
        D{i}.inv{val}.gainmat = ['spm_gainmat_mesh' num2str(Msize) '.mat'];
        D{i}.inv{val}.forward.channels = D{i}.inv{val}.forward.sensors.label';
        D{i}.inv{val}.forward.scale = 1;
    end
end

for i = 1:length(D)
%     D{i}.inv{val}.inverse.trials = D{i}.condlist; % Trials
    D{i}.inv{val}.inverse.type   = method;      % Priors on sources MSP, LOR or IID
    D{i}.inv{val}.inverse.smooth = 0.6;        % Smoothness of source priors (mm)
    D{i}.inv{val}.inverse.Np     = 64;         % Number of sparse priors (x 1/2)

    % and finally, invert
    %--------------------------------------------------------------------------
    D{i}.inv{val}.contrast.woi  = [woi(1) woi(2)];   % peristimulus time (ms)
    D{i}.inv{val}.contrast.fboi = 0;%[foi(1) foi(2)];      % frequency window (Hz)
    D{i}.inv{val}.contrast.type = 'evoked';
    D{i}.save;
end

if groupinversion
    for i = 1:length(D)
        ginv(i,:) = convertCharsToStrings(plist{i});
    end
    spm_eeg_inv_group(char(ginv))

else
    if length(D) == 1
        D{:} = spm_eeg_invert(D{:});
    else
        D = spm_eeg_invert(D);
    end
end
% save gainmatrix if it didnt already exist
if ~(exist(D{1}.inv{val}.gainmat)==2)
    try
        pth=which(D{1}.inv{val}.gainmat);
        copyfile(pth, spmfiles)
    catch
        disp('storing gainmat in myFiles/SPM/ didnt work')
    end
end

if ~groupinversion
    %% save the data struct with the computed inverse solution
    save([temppath 'tempfile.mat'],'D')

    for i = 1:length(D)
        D2 = D{i}.copy([temppath ALLEEG(i).subject '.mat']);
    end
    % create source structure so fieldtrip can use them
    if length(D) > 1
        for i = 1:length(D)
            J = D{i}.inv{1}.inverse.J;
            if numel(J) == 2
                T = D{i}.inv{1}.inverse.T;
                sources_A(i).avg.pow = J{1}*T';
                sources_B(i).avg.pow = J{2}*T';
                if iscell(raw(i).time)
                    sources_A(i).time = raw(i).time{1};
                    sources_B(i).time = raw(i).time{1};
                end
                sources_A(i).pos = D{i}.inv{1}.mesh.tess_mni.vert;
                sources_A(i).tri =  D{i}.inv{1}.mesh.tess_mni.face;
                sources_A(i).inside = D{i}.inv{1}.inverse.Is;
                sources_A(i).method = 'average';

                sources_B(i).pos = D{i}.inv{1}.mesh.tess_mni.vert;
                sources_B(i).tri =  D{i}.inv{1}.mesh.tess_mni.face;
                sources_B(i).inside = D{i}.inv{1}.inverse.Is;
                sources_B(i).method = 'average';

                sources_A_B(i) = sources_A(i);
                sources_A_B(i).avg.pow = sources_A(i).avg.pow - sources_B(i).avg.pow;
            elseif numel(J) == 1
                T = D{i}.inv{1}.inverse.T;
                sources_A(i).avg.pow = J{1}*T';

                if iscell(raw(i).time)
                    sources_A(i).time = raw(i).time{1};
                end
                sources_A(i).pos = D{i}.inv{1}.mesh.tess_mni.vert;
                sources_A(i).tri =  D{i}.inv{1}.mesh.tess_mni.face;
                sources_A(i).inside = D{i}.inv{1}.inverse.Is;
                sources_A(i).method = 'average';
                sources_A_B = [];
                sources_B = [];
            end
        end
    elseif length(D) == 1
        D = D{1};
        J = D.inv{1}.inverse.J;
        if numel(J) == 2
            T = D.inv{1}.inverse.T;
            sources_A(i).avg.pow = J{1}*T';
            sources_B(i).avg.pow = J{2}*T';
            if iscell(raw(i).time)
                sources_A(i).time = raw(i).time{1};
                sources_B(i).time = raw(i).time{1};
            end
            sources_A(i).pos = D.inv{1}.mesh.tess_mni.vert;
            sources_A(i).tri =  D.inv{1}.mesh.tess_mni.face;
            sources_A(i).inside = D.inv{1}.inverse.Is;
            sources_A(i).method = 'average';

            sources_B(i).pos = D.inv{1}.mesh.tess_mni.vert;
            sources_B(i).tri =  D.inv{1}.mesh.tess_mni.face;
            sources_B(i).inside = D.inv{1}.inverse.Is;
            sources_B(i).method = 'average';

            sources_A_B(i) = sources_A(i);
            sources_A_B(i).avg.pow = sources_A(i).avg.pow - sources_B(i).avg.pow;
        elseif numel(J) == 1
            T = D.inv{1}.inverse.T;
            sources_A(i).avg.pow = J{1}*T';
            if iscell(raw(i).time)
                sources_A(i).time = raw(i).time{1};
            end
            sources_A(i).pos = D.inv{1}.mesh.tess_mni.vert;
            sources_A(i).tri =  D.inv{1}.mesh.tess_mni.face;
            sources_A(i).inside = D.inv{1}.inverse.Is;
            sources_A(i).method = 'average';
            sources_A_B = [];
            sources_B = [];

        end

    end
    path(p)
    % save Nifti files for the specified contrast
    if length(D) == 1
        D.inv{D.val}.contrast.smooth  = 8; % FWHM (mm)
        D.inv{D.val}.contrast.display = 0;
        D.inv{val}.contrast.woi  = [woi(1) woi(2)];   % peristimulus time (ms)
        D.inv{val}.contrast.fboi = 0;%[foi(1) foi(2)];      % frequency window (Hz)
        D.inv{val}.contrast.type = 'evoked';
        D.save
        if ~exist([ALLEEG(i).filepath '/spmcontrasts'])
            mkdir([ALLEEG(i).filepath '/spmcontrasts'])
        end
        D2 = D.copy([ALLEEG(i).filepath '/spmcontrasts/' ALLEEG(i).subject '.mat']);
        D2 = spm_eeg_inv_results(D2);
        D2 = spm_eeg_inv_Mesh2Voxels(D2);

    else
        for i = 1:length(D)
            D{i}.inv{D{i}.val}.contrast.smooth  = 8; % FWHM (mm)
            D{i}.inv{D{i}.val}.contrast.display = 0;
            D{i}.inv{val}.contrast.woi  = [woi(1) woi(2)];   % peristimulus time (ms)
            D{i}.inv{val}.contrast.fboi = 0;%[foi(1) foi(2)];      % frequency window (Hz)
            D{i}.inv{val}.contrast.type = 'evoked';
            D{1}.save
            if ~exist([ALLEEG(i).filepath '/spmcontrasts'])
                mkdir([ALLEEG(i).filepath '/spmcontrasts'])
            end
            D2 = D{i}.copy([ALLEEG(i).filepath '/spmcontrasts/' ALLEEG(i).subject '.mat']);
            D2 = spm_eeg_inv_results(D2);
            D2 = spm_eeg_inv_Mesh2Voxels(D2);
        end
    end
    nifti2fieldtrip([ALLEEG(i).filepath '/spmcontrasts' ])
elseif groupinversion
    clear D
    for i = 1:length(ALLEEG)
        D = spm_eeg_load(plist{i});
        J = D.inv{1}.inverse.J;
        if numel(J) == 2
            T = D.inv{1}.inverse.T;
            sources_A(i).avg.pow = J{1}*T';
            sources_B(i).avg.pow = J{2}*T';
            if iscell(raw(i).time)
                sources_A(i).time = raw(i).time{1};
                sources_B(i).time = raw(i).time{1};
            end
            sources_A(i).pos = D.inv{1}.mesh.tess_mni.vert;
            sources_A(i).tri =  D.inv{1}.mesh.tess_mni.face;
            sources_A(i).inside = D.inv{1}.inverse.Is;
            sources_A(i).method = 'average';

            sources_B(i).pos = D.inv{1}.mesh.tess_mni.vert;
            sources_B(i).tri =  D.inv{1}.mesh.tess_mni.face;
            sources_B(i).inside = D.inv{1}.inverse.Is;
            sources_B(i).method = 'average';

            sources_A_B(i) = sources_A(i);
            sources_A_B(i).avg.pow = sources_A(i).avg.pow - sources_B(i).avg.pow;
        elseif numel(J) == 1
            T = D.inv{1}.inverse.T;
            sources_A(i).avg.pow = J{1}*T';

            if iscell(raw(i).time)
                sources_A(i).time = raw(i).time{1};
            end
            sources_A(i).pos = D.inv{1}.mesh.tess_mni.vert;
            sources_A(i).tri =  D.inv{1}.mesh.tess_mni.face;
            sources_A(i).inside = D.inv{1}.inverse.Is;
            sources_A(i).method = 'average';
            sources_A_B = [];
            sources_B = [];
        end
    end
end
path(p)       
    
    
disp('SPM EEG Inversion complete.')


end
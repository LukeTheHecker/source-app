function source_diff = sourcediff(source_A,source_B)

source_diff = source_A;
% Diff of power
source_diff.avg.pow = source_A.avg.pow-source_B.avg.pow;

if isfield(source_A.avg,'mom')
    if ~isempty(source_A.avg.mom)
        % Diff of dipole moment
        for i = 1:numel(source_A.inside)
            source_diff.avg.mom{i} = source_A.avg.mom{i}-source_B.avg.mom{i};
        end
    end
end

end
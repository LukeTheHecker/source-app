function stat_AB = sourcestatistics_simple(app,EEG)
% Calculate a simple source statistic which is most useful for
% mesh/surface-based source inverseions.

% load files & initialize some values
disp('Loading...')
sources_A = load(app.EditField_7.Value);
fn = fieldnames(sources_A);
sources_A = sources_A.(fn{1});

sources_B = load(app.EditField_8.Value);
fn = fieldnames(sources_B);
sources_B = sources_B.(fn{1});

time_range = [app.StartEditField_10.Value,app.StopEditField_10.Value];
norm = app.NormalizeSubjectwiseCheckBox_2.Value;
unsigned = app.UnsignedDatathisistrueforMSPSourcesCheckBox_2.Value;

if strcmp(app.SamplesSwitch_2.Value,'Dependent')
    dep = true;
elseif strcmp(app.SamplesSwitch_2.Value,'Independent')
    dep = false;
end

% time2pnts
[~,lo] = min(abs(sources_A(1).time-(time_range(1)/1000)));
[~,hi] = min(abs(sources_A(1).time-(time_range(2)/1000)));
if hi > size(sources_A(1).avg.pow,2)
    hi = size(sources_A(1).avg.pow,2);
end
% mean across timerange
sources_A2 = sources_A;
sources_B2 = sources_B;
for i = 1:length(sources_A)
    sources_A2(i).avg.pow = mean(sources_A2(i).avg.pow(:,lo:hi),2);
    sources_B2(i).avg.pow = mean(sources_B2(i).avg.pow(:,lo:hi),2);
    A(:,i) = sources_A2(i).avg.pow;
    B(:,i) = sources_B2(i).avg.pow;
end

A(isnan(A)) = 0;
B(isnan(B)) = 0;

% Normalize
if norm
    disp('Normalizing...')
    for i = 1:size(A,2)
        maxval = max( [A(:,i),B(:,i)] , [], 'all');
        A(:,i) = A(:,i)/maxval;
        B(:,i) = B(:,i)/maxval;
    end
end

% stat
disp('Doing the stats...')
if dep
    for i = 1:size(sources_A2(1).avg.pow,1)
        [h(i),p(i),~,sts] = ttest(A(i,:),B(i,:));
        t(i) = sts.tstat;
        
        if unsigned
            diff = mean(abs(A(i,:))-abs(B(i,:)));
            if diff>0
                t(i) = abs(t(i));
            end
        end
    end
elseif ~dep
    for i = 1:size(sources_A2(1).avg.pow,1)
        [h(i),p(i),~,sts] = ttest2(A(i,:),B(i,:));
        t(i) = sts.tstat;
        
        if unsigned
            diff = mean(abs(A(i,:))-abs(B(i,:)));
            if diff>0
                disp('flipping t value')
                t(i) = abs(t(i));
            end
        end
    end
end

% create fieldtrip-like structure
stat_AB.prob = p';
stat_AB.mask = h';
stat_AB.stat = t';
stat_AB.pos = sources_A(1).pos;
stat_AB.time = 1;
stat_AB.tri = sources_A(1).tri;
stat_AB.inside = ones(size(sources_A(1).inside))';
stat_AB.method = sources_A(1).method;

%% correct for multiple comparisons
% p_crit = 0.05/16;           % alpha criterion divided by independent observations, here: temporal modes.
% newmask = stat_AB.prob<p_crit;
% stat_AB.mask = newmask;


end

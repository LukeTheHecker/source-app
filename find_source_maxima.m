function mymaxmin =  find_source_maxima(source,fun,thresh,sephemi,nsources,iter,posneg)
%% finds the maximum of the field fun in source
% source - fieldtrip source structure
% fun - substructure containing source power, statistic etc
% thresh - cluster only voxels above thresh*maximum voxel
% sephemi - true-> separate clustering for each hemisphere, false|[]->
% whole brain
% nsources - number of expected sources (can be [] or 0 if not specified)
% iter - repetitions of k-Means
% posneg - 'pos' -> only positive voxels, 'neg' -> only negative voxels,
% [] -> all voxels

vox_pos = source.pos;
if isempty(nsources) || nsources == 0
    nclust = 8;
else
    nclust = nsources;
end

if strcmp(fun,'avg.pow')
    vox_vals = source.avg.pow;
else
    vox_vals = source.(fun);
end

if strcmp(posneg,'pos')
    vox_vals(find(vox_vals<0)) = NaN;
elseif strcmp(posneg,'neg')
    vox_vals(find(vox_vals>0)) = NaN;
end

maxvox = max(vox_vals);
if ~isempty(thresh)
    vox_vals(find(vox_vals < (maxvox*thresh))) = NaN;
end
vox_pos(find(isnan(vox_vals)),:) = NaN;

if ~sephemi || isempty(sephemi)
    if ~isempty(nsources) && nsources>0
        disp('Clustering Voxels')
        [IDX,C] = kmeans(vox_pos,nclust,'Replicates',iter);
    else
        [IDX,C] = kmeans_opt(vox_pos,nclust,0.95,iter);
    end
else
    vox_pos_L = vox_pos;
    vox_pos_L(find(vox_pos(:,1)>0),:) = NaN;
    vox_pos_R = vox_pos;
    vox_pos_R(find(vox_pos(:,1)<0),:) = NaN;

    if ~isempty(nsources) && nsources>0
        disp('Clustering Voxels in left hemisphere')
        [IDX_L,C_L] = kmeans_opt(vox_pos_L,nclust,'Replicates',iter);
        disp('Clustering Voxels in right hemisphere')
        [IDX_R,C_R] = kmeans_opt(vox_pos_R,nclust,'Replicates',iter);
    else
        disp('Clustering Voxels in left hemisphere')
        [IDX_L,C_L] = kmeans_opt(vox_pos_L,nclust,0.95,iter);
        disp('Clustering Voxels in right hemisphere')
        [IDX_R,C_R] = kmeans_opt(vox_pos_L,nclust,0.95,iter);
    end
    
end

% figure
% scatter3(vox_pos(:,1),vox_pos(:,2),vox_pos(:,3))
% 
% figure
% cl = {'r.','b.','c.','y.','g.'};
% for i = 1:size(C,1)
%     scatter3(vox_pos(find(IDX==i),1),vox_pos(find(IDX==i),2),vox_pos(find(IDX==i),3))%,cl{i})
%     hold on
% end



if ~sephemi || isempty(sephemi)
    for i = 1:size(C,1)
        membervals = NaN(size(vox_vals));
        membervals(find(IDX==i)) = vox_vals(find(IDX==i));
        [~,maxpos] = max(membervals);
        mymaxmin(i,:) = vox_pos(maxpos,:);
    end
else
    for i = 1:size(C_L,1)
        membervals = NaN(size(vox_vals));
        membervals(find(IDX_L==i)) = vox_vals(find(IDX_L==i));
        [~,maxpos] = max(membervals);
        mymaxmin_L(i,:) = vox_pos(maxpos,:);
    end
    
    for i = 1:size(C_R,1)
        membervals = NaN(size(vox_vals));
        membervals(find(IDX_R==i)) = vox_vals(find(IDX_R==i));
        [~,maxpos] = max(membervals);
        mymaxmin_R(i,:) = vox_pos(maxpos,:);
    end
    mymaxmin = [mymaxmin_L;mymaxmin_R];
end

% figure
% scatter3(vox_pos(:,1),vox_pos(:,2),vox_pos(:,3))
% 
% figure
% cl = {'r.','b.','c.','y.','g.'};
% for i = 1:size(C,1)
%     scatter3(vox_pos(find(IDX==i),1),vox_pos(find(IDX==i),2),vox_pos(find(IDX==i),3))%,cl{i})
%     hold on
% end


%% Check Data Type 
%==========================================================================
% disp('Checking data type')
% if ~isfield(source,'dim')
%     load('standard_mri')
%     cfg=[];
%     cfg.downsample = 5;
%     if isfield(source,'avg')
%         cfg.parameter = fun;
%         fun = 'pow';
%     elseif isfield(source,fun)
%         cfg.parameter = fun;
%     end
%     source = ft_sourceinterpolate(cfg,source,mri);
% end
% if strcmp(fun, 'avg.pow')
%     if size(source.avg.pow,2) > 1
%         source.avg.pow = mean(source.avg.pow(:,102:end),2);
%     end
% elseif numel(strsplit(fun,'.')) == 1
%     if size(source.(fun),2) > 1
%         source.(fun) = mean(source.(fun)(:,102:end),2);
%     end
% end
% pos = source.pos;
% inside = source.inside;
% 
% %% Find 3D Maxima
% %==========================================================================
% mymaxima = get_maxima(source,fun);
% myminima = get_minima(source,fun);
% mymaxmin = [mymaxima;myminima];
% %% Remove maxima that are less than 5 % of the global maximum
% relative_pow = 0.20;
% if strcmp(fun,'avg.pow')
%     maxval = max(abs(source.avg.pow));
%     cnt = 1;
%     mymaxima_tmp = [];
%     for i = 1:size(mymaxmin,1)
%         [~,indx]=ismember(mymaxmin(i,:),source.pos,'rows');
%         if abs(source.avg.pow(indx)) >= maxval*relative_pow
%             mymaxima_tmp(cnt,:) = mymaxmin(i,:);
%             cnt = cnt+1;
%         end
%     end
% else
%     maxval = max(abs(source.(fun)));
%     cnt = 1;
%     mymaxima_tmp = [];
%     for i = 1:size(mymaxmin,1)
%         [~,indx]=ismember(mymaxmin(i,:),source.pos,'rows');
%         if abs(source.(fun)(indx)) >= maxval*relative_pow
%             mymaxima_tmp(cnt,:) = mymaxmin(i,:);
%             cnt = cnt+1;
%         end
%     end
% end
% 
% 
% 
% mymaxmin = mymaxima_tmp;

%% Check if smoothing is required to remove local maxima
%==========================================================================
% cnt = 0;
% if numel(strsplit(fun,'.')) == 2
%     splitfun = strsplit(fun,'.');
%     while size(mymaxmin,1) > 50
%         cnt=cnt+1;
%         fprintf('\nSmoothing 3 neighbouring voxels %dth time\n',cnt)
%         mymaxmin = [];
%         disp('Smoothing Source to remove local maxima')
%         source.(splitfun{1}).(splitfun{2}) = reshape(source.(splitfun{1}).(splitfun{2}), [source.dim]);
%         source.(splitfun{1}).(splitfun{2})(isnan(source.(splitfun{1}).(splitfun{2}))) = 0;
%         source.(splitfun{1}).(splitfun{2}) = smooth3(source.(splitfun{1}).(splitfun{2}),'gaussian',[3, 3, 3]);
%         source.(splitfun{1}).(splitfun{2}) = reshape(source.(splitfun{1}).(splitfun{2}), [prod(source.dim),1]);
%         source.pos = pos;
%         source.inside = inside;
%         mymaxima = get_maxima(source,fun);
%         myminima = get_minima(source,fun);
%         mymaxmin = [mymaxima;myminima];
% 
%         
%     end
% else
%     while size(mymaxmin,1) > 50
%         cnt=cnt+1;
%         fprintf('\nSmoothing 3 neighbouring voxels %dth time\n',cnt)
%         mymaxmin = [];
%         disp('Smoothing Source to remove local maxima')
%         source.(fun) = reshape(source.(fun), [source.dim]);
%         source.(fun)(isnan(source.(fun))) = 0;
%         source.(fun) = smooth3(source.(fun),'gaussian',[3, 3, 3]);
%         source.(fun) = reshape(source.(fun), [prod(source.dim),1]);
%         source.pos = pos;
%         source.inside = inside;
%         mymaxima = get_maxima(source,fun);
%         myminima = get_minima(source,fun);
%         mymaxmin = [mymaxima;myminima];
%         size(mymaxmin,1)
%     end
% end


end
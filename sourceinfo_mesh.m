function sourceinfo_mesh(app,source)
%% % This function extracts maxima from statistical image or Inverse Solution
% output that are mesh-based.
warning off
load('aal.mat')
rm_unsig = app.CheckBox_7.Value;
alphacrit = app.AlphacritEditField_2.Value/100;
iter = app.kMeansiterationsEditField.Value;
pos = source.pos;

if app.PositiveButton_2.Value == 1
    pv = true;
else
    pv = false;
end

if app.NegativeButton_2.Value == 1
    nv = true;
else
    nv = false;
end

if length(source)>1
    error('Source Info for multiple sources not yet implemented. You could calculate an average first.')
end

percthresh = 0.05;

if isfield(source, 'stat')
    myText = 'Source Statistics';
    myText = [myText newline 'Stat. Significant Maxima (highest to lowest T):'];
    
    sourcetype = 'stat';
    fun = 'stat';
elseif isfield(source,'avg')
    myText = 'Source Activation';
    myText = [myText newline 'Maxima:'];
    
    if isfield(source.avg,'pow')
        sourcetype = 'normal';
    end
    fun = {'avg','pow'};
end



if strcmp(sourcetype,'stat')
    maxvox = max(source.stat);
    sparam = source.stat;
    
elseif strcmp(sourcetype,'normal')
    maxvox = max(source.avg.pow);
    sparam = source.avg.pow;
    
end

if rm_unsig && isfield(source,'prob')
    sparam(find(source.prob >= alphacrit)) = NaN;
else
    t_thresh = maxvox*percthresh;
    sparam( find(abs(sparam) < (t_thresh)) ) = NaN;
end

if pv
    sparam(find(sparam<0)) = NaN;
elseif nv
    sparam(find(sparam>0)) = NaN;
end

vox_idx = find(~isnan(sparam));

vox_pos = pos(vox_idx,:);


% Cluster the top 30% value voxels 
% [IDX,C] = kmeans_opt(vox_pos,16,0.85,100);
% left
disp('Clustering Voxels in Left Hemisphere...')
vox_pos_L = vox_pos;
vox_pos_L(find(vox_pos(:,1)>0),:) = NaN;
[IDX_L,C_L] = kmeans_opt(vox_pos_L,8,0.95,iter);
% right
disp('Clustering Voxels in Right Hemisphere...')
vox_pos_R = vox_pos;
vox_pos_R(find(vox_pos(:,1)<0),:) = NaN;
[IDX_R,C_R] = kmeans_opt(vox_pos_R,8,0.95,iter);

% TEST: Plot clusters

% figure
% scatter3(vox_pos(:,1),vox_pos(:,2),vox_pos(:,3))
% 
% figure
% cl = {'r.','b.','c.','y.','g.'};
% for i = 1:size(C_R,1)
%     scatter3(vox_pos(find(IDX_R==i),1),vox_pos(find(IDX_R==i),2),vox_pos(find(IDX_R==i),3))%,cl{i})
%     hold on
% end
% 
% figure
% for i = 1:size(C_L,1)
%     scatter3(vox_pos(find(IDX_L==i),1),vox_pos(find(IDX_L==i),2),vox_pos(find(IDX_L==i),3))%,cl{i})
%     hold on
% end




% Find the maximum in obtained clusters

vox_vals = sparam(vox_idx);
if isfield(source, 'prob')
    vox_p = source.prob(vox_idx);
else
    vox_p = size(vox_vals);
end
% Left
for i = 1:size(C_L,1)
    tmp = NaN(size(vox_idx));
    tmp(find(IDX_L==i)) = vox_p(find(IDX_L==i));
    [val(i),maxidx(i)] = min(abs(tmp));
    label{i} = get_label(vox_pos(maxidx(i),:),atlas);
    myText = [myText newline num2str(round(vox_pos(maxidx(i),:)))...
        sprintf('\t') label{i} sprintf('\t')...
        'T = ' num2str(round(vox_vals(maxidx(i)),2)) sprintf('\t') ...
        'p = ' num2str(vox_p(i))];
end
% Right
clear label val maxidx tmp
for i = 1:size(C_R,1)
    tmp = NaN(size(vox_idx));
    tmp(find(IDX_R==i)) = vox_p(find(IDX_R==i));
    [val(i),maxidx(i)] = min(abs(tmp));
    label{i} = get_label(vox_pos(maxidx(i),:),atlas);
    myText = [myText newline num2str(round(vox_pos(maxidx(i),:)))...
        sprintf('\t') label{i} sprintf('\t')...
        'T = ' num2str(round(vox_vals(maxidx(i)),2)) sprintf('\t') ...
        'p = ' num2str(vox_p(i))];
end

app.TextArea.Value = myText;
disp('done')
warning on
end
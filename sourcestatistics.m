function sourcestat = sourcestatistics(app,EEG)
%% Perform Group Statistic of dependent samples


%% init
disp('Initialize all settings...')
% Which test chosen?
if strcmp(app.StatisticDropDown.Value,'Montecarlo')
    parametric = 'montecarlo';
elseif strcmp(app.StatisticDropDown.Value,'Parametric')
    parametric = 'analytic';
end
%% Dependent or independent samples?
if strcmp(app.SamplesSwitch.Value,'Dependent')
    dep = 'ft_statfun_depsamplesT';
elseif strcmp(app.SamplesSwitch.Value,'Independent')
    dep = 'ft_statfun_indepsamplesT';
end
% Which correction chosen?
if strcmp(app.CorrectionDropDown.Value,'None')
    correctm = 'no';
elseif strcmp(app.CorrectionDropDown.Value,'Max Statistic')
    correctm = 'max';
elseif strcmp(app.CorrectionDropDown.Value,'Cluster')
    correctm = 'cluster';
elseif strcmp(app.CorrectionDropDown.Value,'Bonferroni')
    correctm = 'bonferroni';
elseif strcmp(app.CorrectionDropDown.Value,'Holm')
    correctm = 'holm';
elseif strcmp(app.CorrectionDropDown.Value,'Hochberg')
    correctm = 'Hochberg';
end
% Which Cluster Statistic chosen?
if strcmp(app.ClusterStatisticDropDown.Value,'Maxsum')
    clusterstat = 'maxsum';
elseif strcmp(app.ClusterStatisticDropDown.Value,'Maxsize')
    clusterstat = 'maxsize';
elseif strcmp(app.ClusterStatisticDropDown.Value,'WCM')
    clusterstat = 'wcm';
end

time_range = [app.StartEditField_4.Value,app.StopEditField_4.Value]./1000;
normalize = app.NormalizeSubjectwiseCheckBox.Value;
unsigned = app.UnsignedDatathisistrueforMSPSourcesCheckBox.Value;
% Load Data
disp('Loading data...')
sources_A = load(app.EditField.Value);
fn = fieldnames(sources_A);
sources_A = sources_A.(fn{1});
sources_B = load(app.EditField_4.Value);
fn = fieldnames(sources_B);
sources_B = sources_B.(fn{1});



% average over user-specified time range
disp('Averaging...')
[~,lo] = min(abs(sources_A(1).time-time_range(1)));
[~,hi] = min(abs(sources_A(1).time-time_range(2)));

for i = 1:length(sources_A)
    sources_A(i).time = 1;
    sources_B(i).time = 1;
    if size(sources_A(i).avg.pow,2) > 1
        sources_A(i).avg.pow = squeeze(nanmean(sources_A(i).avg.pow(:,lo:hi),2));
        sources_B(i).avg.pow = squeeze(nanmean(sources_B(i).avg.pow(:,lo:hi),2));

    end
end

% interpolate to a mri grid (required by ft_sourcestatistics)
if isfield(sources_A,'tri')
    fun = 'pow';
    mri  = load('standard_mri.mat');
    mri = mri.mri;
    mri.coordsys = 'mni';
    cfg                 = [];
    cfg.resolution      = 5; %mm
%     cfg.dim             = [18,21,18];
    cfg.dim = [36, 42, 36];
    mri_dwn      =  ft_volumereslice(cfg,mri);
    % Get Data in shape
    cfg = [];
    cfg.parameter = 'avg.pow';
    for i = 1:numel(sources_A)
        sources_A(i).avg.pow = full(sources_A(i).avg.pow);
        sources_B(i).avg.pow = full(sources_B(i).avg.pow);

        if i == 1
            sources_A_int{i} = ft_sourceinterpolate(cfg,sources_A(i),mri_dwn);
            sources_B_int{i} = ft_sourceinterpolate(cfg,sources_B(i),mri_dwn);
        else
            % get the extra information of the first sources_X row 
            sources_A_tmp = sources_A(1);
            sources_A_tmp.avg.pow = sources_A(i).avg.pow;
            sources_A_int{i} = ft_sourceinterpolate(cfg,sources_A_tmp,mri_dwn);
            sources_B_tmp = sources_B(1);
            sources_B_tmp.avg.pow = sources_B(i).avg.pow;
            sources_B_int{i} = ft_sourceinterpolate(cfg,sources_B_tmp,mri_dwn);
        end
    end
else
    fun = 'avg.pow';
    for i = 1:numel(sources_A)
        if i == 1
            sources_A_int{i} = sources_A(i);
            sources_B_int{i} = sources_B(i);
        else
            % get the extra information of the first sources_X row 
            sources_A_tmp = sources_A(1);
            sources_A_tmp.avg.pow = sources_A(i).avg.pow;
            sources_A_int{i} = sources_A_tmp;
            
            sources_B_tmp = sources_B(1);
            sources_B_tmp.avg.pow = sources_B(i).avg.pow;
            sources_B_int{i} = sources_B_tmp;
            
        end
    end
end

% Normalize Data if specified by user
disp('Normalizing...')

if normalize == 1
    for i = 1:numel(sources_A_int)
        if strcmp(fun,'avg.pow')
            maxnorm = max([max(sources_A_int{i}.avg.pow),max(sources_B_int{i}.avg.pow)]);
            try
                sources_A_int{i}.avg.pow = sources_A_int{i}.avg.pow./maxnorm;
                sources_B_int{i}.avg.pow = sources_B_int{i}.avg.pow./maxnorm;
            catch
                sources_A_int{i}.pow = sources_A_int{i}.pow./maxnorm;
                sources_B_int{i}.pow = sources_B_int{i}.pow./maxnorm;
            end
        elseif strcmp(fun,'pow')
            maxnorm = max([max(sources_A_int{i}.pow),max(sources_B_int{i}.pow)]);

            sources_A_int{i}.pow = sources_A_int{i}.pow./maxnorm;
            sources_B_int{i}.pow = sources_B_int{i}.pow./maxnorm;
        end
    end
end

if unsigned
    if strcmp(fun,'avg.pow')
        for i = 1:numel(sources_A_int)
            sources_A_int{i}.avg.pow = abs(sources_A_int{i}.avg.pow);
            sources_B_int{i}.avg.pow = abs(sources_B_int{i}.avg.pow);
        end
    elseif ~contains(fun,'.')
        for i = 1:numel(sources_A_int)
            sources_A_int{i}.(fun) = abs(sources_A_int{i}.(fun));
            sources_B_int{i}.(fun) = abs(sources_B_int{i}.(fun));
        end
        
    end
end
        



% Perform Statistics
disp('Do the statistics...')

cfg=[];
cfg.dim                 = sources_A_int{1}.dim;
cfg.method              = parametric;
cfg.statistic           = dep;
cfg.parameter           = fun;
cfg.correctm            = correctm;
cfg.clusterstatistic    = clusterstat;
cfg.numrandomization    = 2000;
cfg.alpha               = 0.05; %
cfg.tail                = 0;    % 0=two tailed
nsubj=numel(sources_A);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
if strcmp(app.SamplesSwitch.Value,'Dependent')
    cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
end
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)
cfg.spmversion = 'spm12';
sourcestat = ft_sourcestatistics(cfg, sources_A_int{:}, sources_B_int{:});

end
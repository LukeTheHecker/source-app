function average_out = calc_avg(app)
%% This function calculates the arithmetic mean of a structure of sources.
% Input can be e.g. inverse solutions on multiple data sets.


% load the data
disp('Loading data...')
sources = load(app.SourcesEditField.Value);
fn = fieldnames(sources);
sources = sources.(fn{1});

if numel(sources)<2
    error('The input file contains only one source!')
end

% Normalize if specified
if app.CheckBox_4.Value
    disp('Normalizing...')
    for i = 1:numel(sources)
        maxnorm = max(sources(i).avg.pow,[],'all');
        sources(i).avg.pow = sources(i).avg.pow./maxnorm;
    end
end

% Average
disp('Creating Average...')
for i = 1:numel(sources)
    tmp_data(i,:,:) = sources(i).avg.pow;
end
avg_data = squeeze(nanmean(tmp_data,1));

average_out = sources(1);
average_out.avg.pow = avg_data;

disp('Finishing...')
end
function diff = calc_diff(app,EEG)
%% Calculates the difference of source power of condition A and B

disp('Calculating Difference of Source Power')
%% load the data
sources_A = load(app.SourcesAEditField.Value);
fn = fieldnames(sources_A);
sources_A = sources_A.(fn{1});

sources_B = load(app.SourcesBEditField.Value);
fn = fieldnames(sources_B);
sources_B = sources_B.(fn{1});

%% iterate through sets and calculate difference
for i = 1:numel(sources_A)
    sources_A(i).avg.pow = sources_A(i).avg.pow-sources_B(i).avg.pow;
end
diff = sources_A;


end
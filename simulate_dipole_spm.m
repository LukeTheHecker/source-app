function dipsim = simulate_dipole_spm(app,ALLEEG)
% initial stuff
savepath C:\Users\Lukas\Documents\SourcePlugin\temp\pathdef.m


ndip = app.NumberofDipolesEditField.Value;
nsim = app.NoofSimulationsEditField.Value;
mindist = app.MindistancebetweenEditField.Value;
pth_fwdsol = app.PathForwardSolutionEditField_2.Value;
inverse_solution = app.InverseSolutionDropDown_4.Value;
SNR = app.SNREditField.Value;
lambda = app.LambdaEditField_3.Value;

if app.LowButton_2.Value == true
    Msize = 1;
elseif app.MediumButton.Value == true
    Msize = 2;
elseif app.HighButton_2.Value == true
    Msize = 3;
end
    
    
%% Create Data Structure
EEG_tmp = pop_loadset(ALLEEG(1).filename,ALLEEG(1).filepath);
data = eeglab2fieldtrip(EEG_tmp, 'preprocessing');
elec = data.elec;

%% Add mesh, datareg, forward

disp('Multiple Sparse Priors Source Analysis')

% Manage Paths
load('defaultpaths.mat')
p = path;
restoredefaultpath
addpath(defaultpaths.spm12)
cd(defaultpaths.spm12)


pth_sp = app.pth_sp.path;
temppath = [pth_sp,'/temp/']; 
myfilespath = [pth_sp, '/myFiles/'];
spmfiles = [pth_sp, '/myFiles/SPM/']; 
addpath(genpath(pth_sp))
condA = app.ConditionAEditField_2.Value;
condB = app.ConditionBEditField_2.Value;
% addpath('C:\Users\Lukas\Documents\fieldtrip\')
% ft_defaults

% spm eeg
spm('defaults','eeg');

D = spm_eeg_ft2spm(data,temppath);
val               = 1;
D.val             = val;
D.inv{val}.method = 'Imaging';


 
% Use a template head model and associated meshes
%--------------------------------------------------------------------------
% meshfname = ['C:\Users\Lukas\Documents\sourceplugin\myFiles\SPM\mesh_',num2str(Msize),'.mat'];
meshfname = [spmfiles 'mesh_',num2str(Msize),'.mat'];

if ~(exist(meshfname)==2)
    D =  spm_eeg_inv_mesh_ui(D, 1, Msize, 1);
    % store it for later use
    mesh = D.inv{1}.mesh;
    save(meshfname,'mesh')
else
    load(meshfname)
    D.inv{1}.mesh = mesh;
end
for i = 1:length(D)
    D.inv{1}.mesh = mesh;
end

modality = D.modality;
if ~ismember(modality, {'EEG', 'MEG', 'Multimodal'})
    error('Unsupported modality')
end
vert  = D.inv{1}.mesh.tess_mni.vert;
face  = D.inv{1}.mesh.tess_mni.face;


S = [];
S.task = 'defaulteegsens';
S.D = D;
D = spm_eeg_prep(S);

hmfname = [spmfiles 'hm_bem'];

if ~(exist(hmfname)==2)
    D = spm_eeg_inv_datareg_ui(D);
    datareg = D.inv{1}.datareg;
    save(hmfname,'datareg')
else
    load(hmfname)
    D.inv{1}.datareg = datareg;
end

% Compute a forward model
%==========================================================================
% Next, using the geometry of the head model and the location of registered 
% sensors, we can now compute a forward model for each dipole and save it in a 
% lead-field or gain matrix.  This is the basis of our likelihood model.
%--------------------------------------------------------------------------
% Without UI:
% lffname = 'C:\Users\Lukas\Documents\sourceplugin\myFiles\SPM\lf_bem.mat';
lffname = [spmfiles 'lf_bem.mat'];
D.inv{1}.forward(1).voltype = 'EEG BEM';

if ~(exist(lffname)==2)
    D = spm_eeg_inv_forward(D);

    forward = D.inv{1}.forward;
    save(lffname,'forward')
else
    load(lffname)
    D.inv{1}.forward = forward;
end
for i = 1:length(D)
    D.inv{1}.forward = forward;
end

%% Start Simulations and reconstructions:
f = waitbar(0,'please wait...');

for isim = 1:nsim
    %% 2. GENERATION OF SOURCES OF NEURAL ACTIVITY
    % SPM works in mm, lead fields are correctly calculated in SI
    % units (spm_cond_units)
    waitbar(isim/nsim, f, sprintf('Simulation %d/%d',isim,nsim))
    meshsourceind{isim} = [];
    Nsources = app.NumberofDipolesEditField.Value;						% Select one, two, or three sources

%     tmp_dists = [];
    for idip = 1:Nsources
        % first dipole:
        if idip==1
            tmp_dip = randsample(size(vert,1),1);
        elseif idip>1
            % try to find a dipole with a distance equal to or greater than
            % mindist
            while tmp_dist < mindist
                tmp_dip = randsample(size(vert,1),1);
                for k = 1:idip-1
                    tmp_dists(k) = sqrt(sum((vert(tmp_dip,:)-vert(meshsourceind{isim}(:,k),:)).^2));
                end
                tmp_dist = min(tmp_dists);
%                 disp('re-searching')
            end
        end
        meshsourceind{isim} = [meshsourceind{isim},tmp_dip];
        tmp_dist = -9999;
    end
    
%     meshsourceind = randsample(size(D.inv{val}.mesh.tess_mni.vert,1),Nsources)';%3714;			% Surce position locator in ctf space
        
    dipfreq = (8-3).*rand(1,3)+3;					% Source frequency
    dipamp = repmat(1000,1,Nsources).*1e-9;			% Source amplitude in nAm


    Ndip = size(meshsourceind{isim},2);		% Number of dipoles

    %% 3. WAVEFORM FOR EACH SIGNAL

    Ntrials = D.ntrials;				% Number of trials

    % define period over which dipoles are active
    startf1  = 0;					% (sec) start time
    duration = 0.6;					% duration 

    endf1 = duration + startf1;
    f1ind = intersect(find(D.time>startf1),find(D.time<=endf1));

    % Create the waveform for each source
    signal{isim} = zeros(Ndip,length(D.time));
    for j=1:Ndip				% For each source
        for i=1:Ntrials			% and trial
            f1 = dipfreq(j);	% Frequency depends on stim condition
            amp1 = dipamp(j);	% also the amplitude
            phase1 = pi/2;
            signal{isim}(j,f1ind) = signal{isim}(j,f1ind)...
                + amp1*sin((D.time(f1ind)...
                - D.time(min(f1ind)))*f1*2*pi + phase1);
        end
    end

    %% 4. CREATE A NEW FORWARD MODEL

    fprintf('Computing Gain Matrix: ')
    spm_input('Creating gain matrix',1,'d');	% Shows gain matrix computation
    
    % load gainmatrix if it already exists from previous inversions:
    if exist([spmfiles 'spm_gainmat_mesh' num2str(Msize) '.mat']) == 2
        lf_folder = fileparts(which(['spm_gainmat_mesh' num2str(Msize) '.mat']));
        lf_fullpath = [lf_folder '/spm_gainmat_mesh' num2str(Msize) '.mat'];
    %     lf = load(lf_fullpath);
        D.inv{val}.gainmat = ['spm_gainmat_mesh' num2str(Msize) '.mat'];
        D.inv{val}.forward.channels = D.inv{val}.forward.sensors.label';
        D.inv{val}.forward.scale = 1;
    end
    
    [L D] = spm_eeg_lgainmat(D);				% Gain matrix
    
    % save gainmatrix if it didnt already exist
    if ~exist([spmfiles 'spm_gainmat_mesh' num2str(Msize) '.mat'])
        load(D.inv{val}.gainmat);
        save([spmfiles 'spm_gainmat_mesh' num2str(Msize) '.mat'],'G','label')
    end

    Nd    = size(L,2);							% number of dipoles
    X	  = zeros(Nd,numel(data.time{1}));						% Matrix of dipoles
    fprintf(' - done\n')

    % Green function for smoothing sources
    fprintf('Computing Green function from graph Laplacian:\n')
   
    A     = spm_mesh_distmtx(struct('vertices',vert,'faces',face),0);
    GL    = A - spdiags(sum(A,2),0,Nd,Nd);
    GL    = GL*0.6/2;
    Qi    = speye(Nd,Nd);
    QG    = sparse(Nd,Nd);
    for i = 1:8
        QG = QG + Qi;
        Qi = Qi*GL/i;
    end
    QG    = QG.*(QG > exp(-8));
    QG    = QG*QG;
    clear Qi A GL
    fprintf(' - done\n')

    % Add waveform of all smoothed sources to their equivalent dipoles
    % QGs add up to 0.9854
    for j=1:Ndip 
        for i=1:numel(data.time{1})
            X(:,i) = X(:,i) + signal{isim}(j,i)*QG(:,meshsourceind{isim}(j));
        end
    end

    D(:,:,1) = L*X;					% Forward model

    % Copy data to all trials (they should not be the same, of course)
    for i=1:Ntrials
        D(:,:,i) = D(:,:,1);
    end

    %% 5. ADD WHITE NOISE
    % In decibels
    % Note: This noise is easier to implement but not realistic
    % in practice the noise power must be channel specific
    for i = 1:size(D,1)
        channoise = std(D(i,:,1)).*randn(size(D(i,:,1)))/(10^(SNR/20));
        D(i,:,1) = D(i,:,1) + channoise;
    end

    %% 6. PLOT AND SAVE

%     Nj		= size(vert,1);
%     M		= mean(X(:,f1ind)'.^2,1);
%     G		= sqrt(sparse(1:Nj,1,M,Nj,1));
%     Fgraph	= spm_figure('GetWin','Graphics');
%     j		= find(G);
% 
%     clf(Fgraph)
%     figure(Fgraph)
%     spm_mip(G(j),vert(j,:)',6);
%     axis image
%     title({sprintf('Generated source activity')});
%     drawnow
% 
%     figure
%     hold on
%     aux = L(1,:)*X;
%     plot(D.time,D(1,:,1));
%     hold on
%     plot(D.time,aux,'r');
%     title('Measured activity over MLC11');
%     legend('Noiseless','Noisy');
    
    D.save;

    fprintf('\n Finish\n')

    %% Inverse Solution
    switch app.InverseSolutionDropDown_4.Value
        case 'Multiple Sparse Priors (MSP)'
            method = 'MSP';
        case 'Greedy Search (MSP)'
            method = 'GS';
        case 'Automatic Relevance Determination (MSP)'
            method = 'ARD';
        case 'LORETA (SPM)'
            method = 'LOR';
        case 'MNE (SPM)'
            method = 'IID';
        case 'COH (SPM)'
            method = 'COH';
        case 'EBB (SPM)'
            method = 'EBB';
        case 'MSP (SPM)'
            method = 'MSP';
        case 'BMR (SPM)'
            method = 'BMR';
    end

    D.inv{val}.inverse.trials = D.condlist; % Trials
    D.inv{val}.inverse.type   = method;      % Priors on sources MSP, LOR or IID
    D.inv{val}.inverse.smooth = 0.6;        % Smoothness of source priors (mm)
    D.inv{val}.inverse.Np     = 64;         % Number of sparse priors (x 1/2)

    % and finally, invert
    %--------------------------------------------------------------------------
    D.inv{val}.contrast.woi  = [200 300];   % peristimulus time (ms)
    D.inv{val}.contrast.fboi = [0 0];      % frequency window (Hz)
    D.inv{val}.contrast.type = 'evoked';

    D = spm_eeg_invert(D);
%     D = spm_eeg_inv_results(D)
    path(p)

    % time course
    J = D.inv{1}.inverse.J{1};
    T = D.inv{1}.inverse.T;
    timecourse = J*T';
    timecourse_max = max(timecourse,[],2);

    % transform it to fieldtrip ft_sourceanalysis structure
    source.avg.pow = timecourse_max;
    source.time = data.time{1};
    source.pos = D.inv{1}.mesh.tess_mni.vert;
    source.tri =  D.inv{1}.mesh.tess_mni.face;
    source.inside = D.inv{1}.inverse.Is;
    source.method = 'average';

    if Nsources == 1
        [~,idx] = max(timecourse_max);
        est_source = vert(idx,:);
        loc_err(isim) = eucl(est_source,vert(meshsourceind{isim},:));
    elseif Nsources > 1
        try
            m = find_source_maxima(source,'avg.pow',0.2,0,ndip,5,'pos');
        catch
            try
                m = find_source_maxima(source,'avg.pow',0.1,0,ndip,5,'pos');
            catch
                m = find_source_maxima(source,'avg.pow',0.01,0,ndip,5,'pos');
            end
        end
              
            
        % Check which maxmimum is closest to first and second dipole
        for k = 1:size(meshsourceind{isim},2)
            for j = 1:size(m,1)
                max_eucl(k,j) = eucl(m(j,:), vert(meshsourceind{isim}(k),:));
            end
        end
        loc_err(isim) = mean(min(max_eucl,[],2));
        max_eucl = [];
        m = [];
    end
end
close(f) % close wait bar

%% Calculate some metrics for results
%% Summarize Results

% Get Dipole Depth (min distance between each simulated dipole and
% electrodes)
for i = 1:nsim
    for j = 1:ndip
        tmp_pos = vert(meshsourceind{i}(j),:);
        depth(i,j) = min(eucl(elec.pnt,tmp_pos));
    end
end
% calculate mean source depth per simulation
depth = mean(depth,2)';
% calculate correlation between mean source depth and localization error
depthcorr = min(corrcoef(depth,loc_err),[],'all');

% Get average distance between dipoles of each simulation
for i = 1:nsim
    % distance between each pair of simulated dipoles
    for j = 1:ndip
        for k = 1:ndip
            tmp_pos1 = vert(meshsourceind{i}(j),:);
            tmp_pos2 = vert(meshsourceind{i}(k),:);
            distmat(j,k) = eucl(tmp_pos1,tmp_pos2);
        end
    end
    lowtri = tril(distmat,-1);
    avgdist(i) = mean(lowtri(find(lowtri~=0)));
end
distcorr = min(corrcoef(avgdist,loc_err),[],'all');

% Get average correlation between source activity in each simulation
for i = 1:nsim
    % distance between each pair of simulated dipoles
    for j = 1:ndip
        for k = 1:ndip
             signalcorr(j,k) = abs(min(corrcoef(signal{i}(j,:),signal{i}(k,:)),[],'all'));
        end
    end
    lowtri = tril(signalcorr,-1);
    avgsignalcorr(i) = mean(lowtri(find(lowtri~=0)));
end
sig_corr_loc_corr = min(corrcoef(avgsignalcorr,loc_err),[],'all');

%% Print results to text area
format short g
results = sprintf('%d simulations of %d randomly set dipole(s). SNR: %d, Minimum Distance: %d mm. Inverse Solution: %s\nMean Localisation Error: %.2f mm +- %.2f mm\nCorrelation [Source Depth X Localisation Error]: %.2f \nCorrelation [Mean Source Distance X Localisation Error]: %.2f \nCorrelation [inter-source timecourse correlation X Localisation Error]: %.2f',nsim, ndip,SNR,mindist,inverse_solution,round(mean(loc_err),2),round(std(loc_err),2),round(depthcorr,2),round(distcorr,2),round(sig_corr_loc_corr,2));

app.ResultsTextArea.Value = results;



end



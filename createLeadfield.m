function [leadfield,headmodel,sourcemodel] = createLeadfield(cfg)
% createLeadfield builds a head model, source model and computes the 
% leadfield
% 
% use as:
% [leadfield,headmodel,sourcemodel] = createLeadfield( cfg )
% or
% call from Source App GUI (recommended)
%
% Inputs:
%   cfg.pth_sa          =   path to source-app directory
%   cfg.elec            =   electrode structure as used in fieldtrip
%   cfg.check_chanlocs  =   true;   % if chanlocs should be re-checked based
%                                   on templates
%                       =   false;  % if you already have correct locations
%   cfg.hm              =   'BEM';  % for Boundary Element Method
%                       =   'FEM';  % for Finite Element Method
%   cfg.sm              =   'Surface';  % for Surface based Source Model
%                       =   'Grid'; % for Grid based Source Model
%   cfg.gm_only         =   1;  % search sources only in gray matter voxels
%                       =   0;  % do not use pre-define source locations
%   cfg.grid_res        =   10  % mm; %resolution of source model grid
%                               (default is 10 mm)
%   cfg.sm_surfres      =   'Low' or 'Medium' or 'High'     % sets source model
%                                                           resolution
%   cfg.hm_res          = 5;    %mm head model resolution in mm (default: 5)
%   cfg.lf_norm         = false or true     % Leadfield Normalization
% Outputs:
%   leadfield           - Leadfield (or sometimes called gainmatrix)
%   headmodel           - Head Model
%   sourcemodel         - Source Model
% See also: 
%  -

% Copyright (C) <2019>  <Lukas Hecker>
%


if nargin < 1
	help createLeadfield;
	return;
end


% get path of sourceplugin
cfg = checkinput(cfg);
if cfg.check_chanlocs
    elec_template = ft_read_sens('standard_1020.elc');
    cfg.elec = align_electrodes(cfg.elec,elec_template);
end
% Manage Paths
pth_myfiles = [cfg.pth_sa, '/myFiles/'];

% Other settings not specifiable by user:
hm_shift    = 0.3;
hm_conduct  = [0.33 0.14 1.79 0.01 0.43];
sm_smooth   = 10;


fprintf('Headmodel: %s, Sourcemodel: %s\n', cfg.hm, cfg.sm)

%% Properties of the Leadfield, Head Model & Source Model to save later
info.hm = cfg.hm;
info.sm = cfg.sm;
info.gm_only = cfg.gm_only;
info.grid_res = cfg.grid_res;


%% create a headmodel
if strcmp(cfg.hm,'BEM')

    if exist('standard_bem.mat')==2
        headmodel = load('standard_bem.mat');
    else 
        error('Could not find a template head model. Make sure you have the full copy of fieldtrip (not fieldtrip lite) in your path.')
    end
    headmodel = headmodel.vol;
elseif strcmp(cfg.hm,'FEM')
    if exist('FEM.mat')==2
        headmodel = load('FEM.mat');
        headmodel = headmodel.headmodel;
    else
%         mri             = ft_read_mri(spm_canonical_T1);
%         mri             = mri.mri;
%         mri.coordsys    = 'mni';
        
        mri             = load('standard_mri.mat');
        mri             = mri.mri;
        mri.coordsys    = 'mni';
%         % get rid of the artifacts
        mri.anatomy(:,:,175:end) = 0;
        mri.anatomy(:,134:end,168:end) = 0;
        mri.anatomy(:,160:end,160:end) = 0;
        mri.anatomy(:,148:end,165:end) = 0;
        mri.anatomy(115:end,92:117,170:end) = 0;
        mri.anatomy(:,12:end,172:end) = 0;
        mri.anatomy(1:85,134:149,164:168) = 0;
        mri.anatomy(140:end,90:115,160:170) = 0;
        % plot
        cfg_plot             = [];
        ft_sourceplot(cfg_plot,mri);

        cfg_volumeseg             = [];
        cfg_volumeseg.output      = {'gray','white','csf','skull','scalp'};
        seg = ft_volumesegment(cfg_volumeseg,mri);

        % VolumeDownsample
        cfg_voldownsample             = [];
        cfg_voldownsample.smooth      = 12; 
        cfg_voldownsample.downsample  = cfg.hm_res;
        cfg_voldownsample.spmversion = 'spm12';
        seg = ft_volumedownsample(cfg_voldownsample,seg);
%         plot the segmented brain:
        seg_i = ft_datatype_segmentation(seg,'segmentationstyle','indexed');

        cfg_souceplot              = [];
        cfg_souceplot.funparameter = 'seg';
        cfg_souceplot.funcolormap  = lines(6); % distinct color per tissue
        cfg_souceplot.location     = 'center';
        cfg_souceplot.atlas        = seg_i;    % the segmentation can also be used as atlas
        ft_sourceplot(cfg_souceplot, seg_i);

        % Create Mesh
        cfg_prepmesh        = [];
        cfg_prepmesh.shift  = hm_shift;
        cfg_prepmesh.method = 'hexahedral';
        mesh = ft_prepare_mesh(cfg_prepmesh,seg);
        figure
        ft_plot_mesh(mesh)
        % Create Headmodel
        cfg_prephm                 = [];
        cfg_prephm.method          ='simbio';
        % Conductivity Values taken from http://www.fieldtriptoolbox.org/tutorial/headmodel_eeg_fem/
        cfg_prephm.conductivity    = hm_conduct;   % order follows mesh.tissuelabel
        headmodel           = ft_prepare_headmodel(cfg_prephm, mesh);
        if ~exist('FEM.mat')
            save([cfg.pth_sa,],'headmodel')
        end
    end
end



%% create sourcemodel
if strcmp(cfg.sm,'Surface')
    sourcemodel = ft_read_headshape(cfg.surffile);
elseif strcmp(cfg.sm,'Grid')
    if exist('standard_mri.mat')==2
        mri = load('standard_mri.mat');
    else
        error('Could not find the template MRI image "standard_mri.mat". Make sure you have the full copy of fieldtrip (not fieldtrip lite) in your path.')
    end
    mri             = mri.mri;
    mri.coordsys    = 'mni';    
    % Segment standard MRI to get gray matter locations
    seg_sm = ft_volumesegment([],mri);
    cfg_voldownsample = [];
    cfg_voldownsample.downsample = cfg.grid_res;
    cfg_voldownsample.parameter = 'all';
    cfg_voldownsample.smooth = sm_smooth; % or some other value
    cfg_voldownsample.spmversion = 'spm12';
    seg_sm = ft_volumedownsample(cfg_voldownsample, seg_sm);
    if cfg.gm_only == 0
        [X,Y,Z] = ndgrid(1:seg_sm.dim(1), 1:seg_sm.dim(2), 1:seg_sm.dim(3));
        grid = [];
        grid.pos = ft_warp_apply(seg_sm.transform, [X(:) Y(:) Z(:)]); clear X Y Z
        grid.dim = seg_sm.dim;
        % Inside if likelihood of the voxel being grey matter, white
        % matter or csf is larger than 50%
        disp('Searching grid points')
        grid.inside = find((seg_sm.gray+seg_sm.white+seg_sm.csf)>0.5);
        grid.outside = find((seg_sm.gray+seg_sm.white+seg_sm.csf)<=0.5);

        grid = ft_convert_units(grid, 'mm');

        sourcemodel = grid;
    elseif cfg.gm_only == 1

        % downsample to preferred resolution taking only gray matter
        
    
    % create grid
    [X,Y,Z] = ndgrid(1:seg_sm.dim(1), 1:seg_sm.dim(2), 1:seg_sm.dim(3));
    grid = [];
    grid.pos = ft_warp_apply(seg_sm.transform, [X(:) Y(:) Z(:)]); clear X Y Z
    grid.dim = seg_sm.dim;
    % Gray matter is defined here as it is in the LORETA Key software of
    % Roberto D. Pasqual-Marqui (https://www.uzh.ch/keyinst/NewLORETA/Software/Software.htm)
    grid.inside = find(seg_sm.gray>0.33 & seg_sm.gray>seg_sm.white & seg_sm.gray>seg_sm.csf);
    grid.outside = find(seg_sm.gray<=0.33 | seg_sm.gray<=seg_sm.white | seg_sm.gray<=seg_sm.csf);

    grid = ft_convert_units(grid, 'mm');
    
    sourcemodel = grid;
    end    
end

%% Plot if desired
if strcmp(cfg.sm,'Surface')==1
    figure; hold on;
    ft_plot_vol(headmodel, 'edgecolor', 'none', 'facealpha', 0.2);
    ft_plot_mesh(sourcemodel);
    ft_plot_sens(cfg.elec, 'style', '*b');
    title('My Head and Source Model')
elseif strcmp(cfg.sm,'Grid')==1
    figure; hold on;
    ft_plot_vol(headmodel, 'edgecolor', 'none', 'facealpha', 0.2);
    ft_plot_mesh(grid.pos(grid.inside,:));
    ft_plot_sens(cfg.elec, 'style', '*b');
    title('My Head and Source Model')
end
%% Create Leadfield
cfg_prepleadfield = [];
cfg_prepleadfield.sourcemodel        = sourcemodel;
cfg_prepleadfield.headmodel   = headmodel;
cfg_prepleadfield.normalize   = cfg.lf_norm;
cfg_prepleadfield.elec        = cfg.elec;
cfg_prepleadfield.reducerank  = 3;
leadfield       = ft_prepare_leadfield(cfg_prepleadfield);

% Save headmodel sourcemodel & leadfield
f = pth_myfiles;
uisave({'headmodel', 'sourcemodel','leadfield'},[f cfg.hm '_' cfg.sm '_' 'leadfield']);

end

function cfg=checkinput(cfg)
%% This function checks whether necessary input parameters were entered and
% sets default values when possible
    if ~isfield(cfg,'pth_sa')
        pth_sa=what('source-app');
        if ~isempty(pth_sa.path) && (exist(pth_sa.path)==7)
            cfg.pth_sa = pth_sa;
        else
            f=msgbox('Source-App path nout found!','Please select the source-app path !');
            uiwait(f)
            cfg.pth_sa = uigetdir([],'Select your Source-App Path !');
        end
    end
    
    if ~isfield(cfg,'elec')
            elec_info = 'standard_1005.elc';
            elec.label = elec_info.label;
            elec.pnt = elec_info.chanpos;    
            cfg.elec = elec;
    end
    
    if ~isfield(cfg, 'hm')
        error('cfg.hm needs to be specified! add: cfg.hm = "BEM" or cfg.hm = "FEM" !')
    end
    if ~isfield(cfg, 'sm')
        error('cfg.sm needs to be specified! add: cfg.sm = "Surface" or cfg.sm = "Grid" !')
    end
    
    if ~isfield(cfg, 'gm_only')
        disp('Not restricting source model to gray matter')
        cfg.gm_only = 0;
    end
    
    if ~isfield(cfg, 'grid_res')
        disp('Using 10 mm grid (source model) resolution')
        cfg.grid_res = 10; % assign default of 10mm grid resolution
    end
    
    if ~isfield(cfg, 'sm_surfres')
        disp('Using low res for Source Model')
        cfg.sm_surfres = 'Low (Default)';
    end
    
    if ~isfield(cfg, 'hm_res')
        disp('Using res = 5mm for Head Model')
        cfg.hm_surfres = 5;
    end
    
    if ~isfield(cfg, 'lf_norm')
        disp('No Leadfield Normalization')
        cfg.lf_norm = 'no';
    else
        cfg.lf_norm = 'yes';
        if strcmp(cfg.sm_surfres,'Low')
            cfg.surffile = 'cortex_5124.surf.gii';
        elseif strcmp(cfg.sm_surfres,'Medium')
            cfg.surffile = 'cortex_8196.surf.gii';
        elseif strcmp(cfg.sm_surfres,'High')
            cfg.surffile = 'cortex_20484.surf.gii';
        end
    end
    
    if ~isfield(cfg, 'check_chanlocs')
        disp('No additional reading of Chanlocs')
        cfg.check_chanlocs = 0;
    end

    disp('Input Check done')
end

function elec = align_electrodes(elec, template)
    %% find correct channel locations based on fieldtrip templates
    pth_ft = what('fieldtrip');
    if isempty(pth_ft.path) || ~exist(pth_ft.path)
        error('Fieldtrip path not in directory ! Please add path using addpath("FieldtripPath") !')
    end
    
    
    %% Try to align Electrodes to any of the template files until it works
    if isempty(template)
        list_elc_templates = dir([pth_ft.path, '/template/electrode/*.elc']);
        for ilist = 1:length(list_elc_templates)
            try
                elec_template = ft_read_sens(list_elc_templates(ilist).name);
                elec = get_chanlocs_from_labels(elec,elec_template);
                template_found = true;
                break
            catch
                continue
            end
        end
    else
        elec = get_chanlocs_from_labels(elec,template);
        template_found = true;
    end
    if ~template_found
        error('Could not find a matching electrode template using all fieldtrip templates! Please Make sure your electrode locations in cfg.elec are correct!')
    else
        try
            disp(fprintf('realigned electrodes based on %s template',list_elc_templates(ilist).name))
        catch
        end
    end
end
function nifti2fieldtrip(pth)

mdir = dir([pth, '/*.nii']);

% Read nifti, put into typical struct, move field 'anatomy' to avg.pow
cntA = 1;
cntB = 1;
for i = 1:length(mdir)
    if mod(i,2)
        tmp_A = ft_read_mri([mdir(i).folder,'/' mdir(i).name]);
        % Downsample to 2 mm voxel distance
        cfg = [];
        cfg.downsample = 2;
        tmp_A = ft_volumedownsample(cfg,tmp_A);
        
        tmp_A.avg.pow = tmp_A.anatomy;
        tmp_A.avg.pow = tmp_A.avg.pow(:);
        tmp_A = rmfield(tmp_A,'anatomy');
        tmp_A = rmfield(tmp_A,'inside');
        tmp_A.time = 1;
        sources_A(cntA)= tmp_A;
        cntA = cntA + 1;
        
    else
        tmp_B = ft_read_mri([mdir(i).folder,'/' mdir(i).name]);
        cfg = [];
        cfg.downsample = 2;
        tmp_B = ft_volumedownsample(cfg,tmp_B);

        tmp_B.avg.pow = tmp_B.anatomy;
        tmp_B.avg.pow = tmp_B.avg.pow(:);
        tmp_B = rmfield(tmp_B,'anatomy');
        tmp_B = rmfield(tmp_B,'inside');
        tmp_B.time = 1;
        sources_B(cntB)= tmp_B;
        cntB = cntB + 1;
    end
end

save([pth,'/sources_A.mat'],'sources_A')
save([pth,'/sources_B.mat'],'sources_B')

end


% 
% [a,b,c,~] = tmp_A.transform*[1 1 1 1]'
% pos = ans(1:3)

function sensorplot(app,ALLEEG)
[settings] = init(app);

% Preprocess data and store average into two matrices
data = pepro(settings,ALLEEG);

if settings.PlotERP == 1
    % Plot ERP
    plot_erp(ALLEEG,data);
end

if settings.PlotSM
    % Plot Scalp Map
    plot_sm(ALLEEG,settings,data)
    
end

if settings.PlotCSD
    % Plot Current Source Density Maps
    plot_csd(ALLEEG,settings,data)
end


end

function [settings] = init(app)
    switch app.PlotERPsCheckBox.Value
        case 1
            settings.PlotERP = 1;
        case 0
            settings.PlotERP = 0;
    end
    
    switch app.PlotScalpMapsCheckBox.Value
        case 1
            settings.PlotSM = 1;
        case 0
            settings.PlotSM = 0;
    end
    
    switch app.PlotCurrentSourceDensityCheckBox.Value
        case 1
            settings.PlotCSD = 1;
        case 0
            settings.PlotCSD = 0;
    end
    
    switch app.CommonAverageRereferencingCheckBox.Value
        case 1
            settings.prepCAR = 1;
        case 0
            settings.prepCAR = 0;
    end
    
    switch app.RemoveBaselineCheckBox.Value
        case 1
            settings.prepRB = 1;
        case 0
            settings.prepRB = 0;
    end
    
    switch app.FilterCheckBox.Value
        case 1
            settings.prepFilt = 1;
        case 0
            settings.prepFilt = 0;
    end
    settings.condA = app.ConditionAEditField_3.Value;
    settings.condB = app.ConditionBEditField_3.Value;
    settings.sf = app.SplineFlexibilityEditField.Value;
    settings.baseline = [app.StartEditField_7.Value, app.StopEditField_7.Value];
    settings.highpass = app.HighpassEditField.Value;
    settings.lowpass = app.LowpassEditField.Value;
    if settings.highpass == 0
        settings.highpass = [];
    end
    if settings.lowpass == 0
        settings.lowpass = [];
    end
    settings.timerange = [app.StartEditField_9.Value, app.StopEditField_9.Value];
    settings.smlatency = [app.StartEditField_8.Value, app.StopEditField_8.Value];
end

function data = pepro(settings,ALLEEG)

for i = 1:length(ALLEEG)
   eeg = pop_loadset(ALLEEG(i).filename,ALLEEG(i).filepath);

   if settings.prepCAR
       eeg = pop_reref(eeg,[]);
   end

   if settings.prepRB
       tmp = eeg.data;
       [~,lo] = min(abs(eeg.times-settings.baseline(1)));
       [~,hi] = min(abs(eeg.times-settings.baseline(2)));
       tmp = rmbase(tmp,[], lo:hi);
       eeg.data = tmp;
   end

   if settings.prepFilt
       eeg = pop_eegfiltnew(eeg,settings.highpass,settings.lowpass,[],0,0,0);
   end

   eeg_a = pop_epoch(eeg,{settings.condA},[settings.timerange(1)/1000 settings.timerange(2)/1000 0 0]);
   eeg_b = pop_epoch(eeg,{settings.condB},[settings.timerange(1)/1000 settings.timerange(2)/1000 0 0]);

   data_a(i,:,:) = squeeze(mean(eeg_a.data,3));
   data_b(i,:,:) = squeeze(mean(eeg_b.data,3));
   
end
    
data.avg_a = squeeze(mean(data_a,1));
data.avg_b = squeeze(mean(data_b,1));
data.time = eeg_a.times;
end

function plot_erp(ALLEEG,data)
   
% Plot Condition A
fig1=figure;
plottopo(data.avg_a, 'chanlocs', ALLEEG(1).chanlocs,'limits',[min(data.time)/1000 max(data.time)/1000 0 0],...
'colors',{{'k' 'linewidth' 1.5 } {'r' 'linewidth' 1.5 }},'ydir',1,...
'showleg','on')
set(fig1, 'PaperPositionMode', 'auto')
sgtitle('Condition A')

% Plot Condition B
fig2=figure;
plottopo(data.avg_b, 'chanlocs', ALLEEG(1).chanlocs,'limits',[min(data.time)/1000 max(data.time)/1000 0 0],...
'colors',{{'k' 'linewidth' 1.5 } {'r' 'linewidth' 1.5 }},'ydir',1,...
'showleg','on')
set(fig2, 'PaperPositionMode', 'auto')
sgtitle('Condition B')

% Plot Condition A - B
fig3=figure;
plottopo(data.avg_a-data.avg_b, 'chanlocs', ALLEEG(1).chanlocs,'limits',[min(data.time)/1000 max(data.time)/1000 0 0],...
'colors',{{'k' 'linewidth' 1.5 } {'r' 'linewidth' 1.5 }},'ydir',1,...
'showleg','on')
set(fig3, 'PaperPositionMode', 'auto')
sgtitle('Condition A - B')
end

function plot_sm(ALLEEG,settings,data)

[~,lo] = min(abs(data.time-settings.smlatency(1)));
[~,hi] = min(abs(data.time-settings.smlatency(2)));
figure
sgtitle('Regular Scalp Maps')
subplot(1,3,1)
topoplot(mean(data.avg_a(:,lo:hi),2),ALLEEG(1).chanlocs)
title('Condition A')
colorbar

subplot(1,3,2)
topoplot(mean(data.avg_b(:,lo:hi),2),ALLEEG(1).chanlocs)
title('Condition B')
colorbar

subplot(1,3,3)
topoplot(mean(data.avg_a(:,lo:hi),2)-mean(data.avg_b(:,lo:hi),2),ALLEEG(1).chanlocs)
title('Condition A - B')
colorbar

end

function plot_csd(ALLEEG,settings,data)

% initialize CSD Parameters

for i = 1:length(ALLEEG(1).chanlocs)
    chans(i) = {ALLEEG(1).chanlocs(i).labels};
end
chans=chans.';
[M] = ExtractMontage ('10-5-System_Mastoids_EGI129.csd', chans);
[G,H] = GetGH(M,settings.sf); %default: 4, otherwise e.g.: GetGH(M, 3)

[~,lo] = min(abs(data.time-settings.smlatency(1)));
[~,hi] = min(abs(data.time-settings.smlatency(2)));

figure
sgtitle('Current Source Density')
subplot(1,3,1)
tmp = mean(data.avg_a(:,lo:hi),2);
tmp_CSD = CSD(tmp,G,H);
topoplot(tmp_CSD,ALLEEG(1).chanlocs)
title('Condition A')
colorbar

subplot(1,3,2)
tmp = mean(data.avg_b(:,lo:hi),2);
tmp_CSD = CSD(tmp,G,H);
topoplot(tmp_CSD,ALLEEG(1).chanlocs)
title('Condition B')
colorbar

subplot(1,3,3)
tmp1 = mean(data.avg_a(:,lo:hi),2);
tmp1_CSD = CSD(tmp1,G,H);
tmp2 = mean(data.avg_b(:,lo:hi),2);
tmp2_CSD = CSD(tmp2,G,H);

topoplot(tmp1_CSD-tmp2_CSD,ALLEEG(1).chanlocs)
% topoplot(CSD(tmp1,G,H)-CSD(tmp2,G,H),ALLEEG(1).chanlocs)
title('Condition A - B')
colorbar

end
function sourceplot(EEG,sourcefile,app)
% load
disp('loading...')
source = load(sourcefile);
fn = fieldnames(source);
source = source.(fn{1});
smooth = app.SmoothSourceEditField.Value;
voxthresh = app.ShowVoxelsaboveEditField.Value/100;
rm_unsig = app.CheckBox_5.Value;
alphacrit = app.AlphacritEditField.Value;
if app.BothButton.Value == 1
    show = 'Both';
elseif app.PositiveButton.Value == 1
    show = 'Positive';
elseif app.NegativeButton.Value == 1
    show = 'Negative';
end
% check whether data is result of 'sourceanalysis' or 'sourcestatistic'
if isfield(source,'avg') && isfield(source.avg,'pow')
    fun = {'avg','pow'};
elseif isfield(source,'sources')
    error('you cant plot multiple sources at once here. You could e.g.average them first.')
elseif isfield(source,'stat')
    fun = 'stat';
end

if numel(fun)==2
        tmpfun = [fun{1} '.' fun{2}];
    else
        tmpfun = fun;
end

disp('plotting...')
if app.SlicesButton.Value == true
    %% SLICES
    source.time = 1;
    if ~iscell(fun)
        if size(source.(fun),1) > 1 && size(source.(fun),2) > 1
            source.(fun) = squeeze(nanmean(source.(fun),2));
        end
    elseif iscell(fun) && numel(fun) == 2
        if size(source.(fun{1}).(fun{2}),1) > 1 && size(source.(fun{1}).(fun{2}),2) > 1
            source.(fun{1}).(fun{2}) = squeeze(nanmean(source.(fun{1}).(fun{2}),2));
        end
    end
    
    mri             = load('standard_mri.mat');
    mri             = mri.mri;
    mri.coordsys    = 'mni';
    cfg             = [];
    cfg.downsample  = 2;
    
    cfg.parameter   = tmpfun;
    source_int      = ft_sourceinterpolate(cfg, source, mri);
    
    if ~isempty(smooth) && smooth > 0
        try
            source_int.(tmpfun) = reshape(source_int.(tmpfun), [source_int.dim]);
            source_int.(tmpfun)(isnan(source_int.(tmpfun))) = 0;
            source_int.(tmpfun) = smooth3(source_int.(tmpfun),'gaussian',[repmat(smooth,1,3)]);
            source_int.(tmpfun) = reshape(source_int.(tmpfun), [prod(source_int.dim),1]);
            
        catch 
            source_int.(fun{2}) = reshape(source_int.(fun{2}), [source_int.dim]);
            source_int.(fun{2})(isnan(source_int.(fun{2}))) = 0;
            source_int.(fun{2}) = smooth3(source_int.(fun{2}),'gaussian',[repmat(smooth,1,3)]);
            source_int.(fun{2}) = reshape(source_int.(fun{2}), [prod(source_int.dim),1]);
            
            
        end
    end
    
    if ~isempty(voxthresh) && voxthresh~=0
        try
            source_int.(tmpfun)(find(abs(source_int.(tmpfun))<(voxthresh*max(abs(source_int.(tmpfun)))))) = NaN;
        catch
            source_int.(fun{2})(find(abs(source_int.(fun{2}))<(voxthresh*max(abs(source_int.(fun{2})))))) = NaN;
        end
    end
    
    cfg = [];
    cfg.method        = 'slice';
    cfg.funparameter  = tmpfun;
    cfg.maskparameter = tmpfun;
    try
        cfg.funcolorlim   = [0, max(source.(tmpfun))];%'zeromax';
        cfg.opacitylim   = [0, max(source.(tmpfun))/10];%'zeromax';
    catch
        cfg.funcolorlim   = [0, max(source.(fun{1}).(fun{2}))];%'zeromax';
        cfg.opacitylim   = [0, max(source.(fun{1}).(fun{2}))/10];%'zeromax';
    end
    cfg.funcolorlim = 'maxabs';
    cfg.opacitylim = 'maxabs';
    cfg.opacitymap    = 'rampup';
    ft_sourceplot(cfg, source_int);
elseif app.SourceMovieButton.Value == true
    %% MOVIE
    % check if mesh is in source
    if ~isfield(source,'tri')
        errordlg('Your source was not computed on a cortical surface, please choose another plot option: Slices,Orthogonal','Plot not supported');
        error('Ending Plot Function...')
    end

    if numel(fun)==2
        tmp = [fun{1} '.' fun{2}];
        source.(fun{2}) = source.(fun{1}).(fun{2});
        tmpfun = fun{2};
    else
        tmpfun = fun;
    end
    
    source.time = source.time(1:size(source.(tmpfun),2));

    source_desc  = ft_sourcedescriptives([],source);
    cfg = [];
    cfg.funparameter = tmpfun;
    ft_sourcemovie(cfg,source_desc);
    
elseif app.SurfaceButton.Value == true
    %% SURFACE
    [~,lo] = min(abs(source.time-(app.StartEditField_3.Value/1000)));
    [~,hi] = min(abs(source.time-(app.StopEditField_3.Value/1000)));
    pnts = lo:hi;
    
     if ~isempty(voxthresh) && voxthresh~=0
        try
            source.(tmpfun)(find(abs(source.(tmpfun))<(voxthresh*max(abs(source.(tmpfun)))))) = 0;
        catch
            source.(fun{1}).(fun{2})(find(abs(source.(fun{1}).(fun{2}))<(voxthresh*max(abs(source.(fun{1}).(fun{2})))))) = 0;
        end
    end
    
    % Mean over specified time
    if numel(source.time) > 1
        if numel(fun)==2
            for i = 1:numel(source.inside)
                tmp_mean(i) = squeeze(nanmean(source.(fun{1}).(fun{2})(i,pnts),2));
            end
            source.(fun{1}).(fun{2}) = tmp_mean';
        else
            for i = 1:numel(source.inside)
                tmp_mean(i) = squeeze(nanmean(source.(fun)(i,pnts),2));
            end
            source.(fun) = tmp_mean';
        end
    end
    
    
    
    
    if ~isfield(source,'tri')
        % errordlg('Your source was not computed on a cortical surface, please choose another plot option: Slices,Orthogonal','Plot not supported');
        % error('Ending Plot Function...')
        cfg = [];
        cfg.method = 'surface';
        cfg.funparameter = fun;
        % Mask Parameter
        if isfield(source,'mask') && numel(find(source.mask==1))>10
            cfg.maskparameter = 'mask';%
        elseif ~isfield(source,'mask')
            cfg.maskparameter = fun;
        else
            disp('You are trying to plot masked source statistics but there are no statistically significant voxels. Plotting unmasked T Values.')
            cfg.maskparameter = fun;
        end
        if ~iscell(fun)
            cfg.funcolorlim = [-max(abs(source.(fun))) max(abs(source.(fun)))];
            if ~isempty(smooth) && smooth > 0
                source.(fun) = reshape(source.(fun), [source.dim]);
                source.(fun)(isnan(source.(fun))) = 0;
                source.(fun) = smooth3(source.(fun),'gaussian',[repmat(smooth,1,3)]);
                source.(fun) = reshape(source.(fun), [prod(source.dim),1]);
            end
            
        elseif iscell(fun) && numel(fun)==2
            cfg.funcolorlim = [-max(abs(source.(fun{1}).(fun{2}))) max(abs(source.(fun{1}).(fun{2})))];
        end
        cfg.funcolorlim = 'maxabs';%[0, max(source.(fun))*0.6]; %'zeromax';
        cfg.opacitylim = 'maxabs';%cfg.funcolorlim;
        cfg.maskparameter = cfg.funparameter;
        cfg.opacitymap = 'rampup';
        cfg.projmethod = 'nearest';
        cfg.surffile       = 'surface_white_both.mat'; % Cortical sheet from canonical MNI brain
        cfg.camlight = 'no';
        cfg.facecolor = 'brain';
        ft_sourceplot(cfg,source)
        material dull
        
        % Left hemisphere
        cfg.surffile       = 'surface_white_left.mat'; % Cortical sheet from canonical MNI brain
        ft_sourceplot(cfg,source)
        material dull
        % Right hemisphere
        cfg.surffile       = 'surface_white_right.mat'; % Cortical sheet from canonical MNI brain
        ft_sourceplot(cfg,source)
        material dull

    else
        
        try
            m=source.(fun)(:,1);
        catch
            m=source.avg.pow(:,1); 
        end

        
        if rm_unsig
            m(find(source.prob >= (alphacrit/100))) = 0;
        end
        
        if strcmp(show,'Positive')
            m(find(m<0)) = 0;
        elseif strcmp(show,'Negative')
            m(find(m>0)) = 0;
        end
        % Plot whole brain
        figure;
        ft_plot_mesh(source, 'vertexcolor', m,'vertexalpha',0.75,'maskstyle','opacity','facecolor','black','facealpha',0.8);
        view([180 0]); h = light; set(h, 'position', [0 1 0.2]); lighting gouraud; material dull
        % Plot left hemisphere
        source_left = source;
        source_left.pos(source_left.pos(:,1)>2,:) = NaN;
        m_l = m;
        m_l(find(isnan(source_left.pos(:,1)))) = NaN;
        view([180 0]); h = light; set(h, 'position', [0 1 0.2]); lighting gouraud; material dull
        figure
        ft_plot_mesh(source,'vertexcolor',m_l)
        view([180 0]); h = light; set(h, 'position', [0 1 0.2]); lighting gouraud; material dull
        % Plot right hemisphere
        source_right = source;
        source_right.pos(source_right.pos(:,1)<-2,:) = NaN;
        m_r = m;
        m_r(find(isnan(source_right.pos(:,1)))) = NaN;
        figure
        ft_plot_mesh(source,'vertexcolor',m_r)
        view([180 0]); h = light; set(h, 'position', [0 1 0.2]); lighting gouraud; material dull
    end

    if isfield(source,'mask') && numel(find(source.mask==1))<10
        fprintf('========IMPORTANT: You are trying to plot masked source statistics but there are only %d statistically\n significant voxels. Plotting unmasked T Values.========\n',numel(find(source.mask==1)))
    end
    
elseif app.OrthogonalButton.Value == true
    %% ORTHO
    if isfield(source,'tri')
        errordlg('Your source was computed on a cortical surface, please choose another plot option: Source Movie,Surface','Plot not supported');
        error('Ending Plot Function...')
    end
    load('aal.mat')
    mri             = load('standard_mri.mat');
    mri             = mri.mri;
    mri.coordsys    = 'mni';
    cfg             = [];
    cfg.downsample  = 2;
    if numel(fun)==2
        tmpfun = [fun{1} '.' fun{2}];
    else
        tmpfun = fun;
    end
    cfg.parameter   = tmpfun;
    source_int      = ft_sourceinterpolate(cfg, source, mri);
 
    if ~isempty(smooth) && smooth > 0
        source_int.(tmpfun) = reshape(source_int.(tmpfun), [source_int.dim]);
        source_int.(tmpfun)(isnan(source_int.(tmpfun))) = 0;
        source_int.(tmpfun) = smooth3(source_int.(tmpfun),'gaussian',[repmat(smooth,1,3)]);
        source_int.(tmpfun) = reshape(source_int.(tmpfun), [prod(source_int.dim),1]);
    end
    % Plot Ortho
    cfg = [];
    cfg.atlas = atlas;
    cfg.method        = 'ortho';
    cfg.funparameter  = tmpfun;
    cfg.maskparameter = tmpfun;
    cfg.funcolorlim   = 'zeromax';
    cfg.opacitylim   = 'zeromax';
    cfg.opacitymap    = 'rampup';
    ft_sourceplot(cfg, source_int);

end

end
function cv_lambda = crossval_lambda(data, cfg_sa,baseline)
%% Cross-validate different lambda for optimized regularisation 
% NOT FINISHED
% error('Cross-validation not available yet!')
% To do: n-fold crossvalidation, i.e. generate training & test set on the
% fly n times for each lambda
% lambda = 1:-0.002:0.002;

n = 2; % n-fold crossvalidation
time_check = 0.200; % check training-test similarity at 0.200 s post-stimulus
[~,time_check_pnt] = min(abs(data.time{1}-time_check));

% start with 5 log-spaced values between ~0.2 and 10e-3
% The values were chosen arbitrarily based on standard values mentioned in 
% the fieldtrip mailing list. 

lambda=logspace(-100,-0.5,5);
lambda=linspace(0,0.5,5);
f = waitbar(0,'Please wait...');
for i = 1:numel(lambda)
    waitbar(i/numel(lambda),f,sprintf('Testing Lambda %d/%d',i,numel(lambda)))
    fprintf('Trying for lambda: %d\n',lambda(i))
    % cross-validation with n random assignments of trials to training and
    % test
    for nfold = 1:n
        fprintf('Cross Validation %d \n',nfold)
        % assign trials to training and test set randomly
        trials = 1:numel(data.trial);
        ntrials = numel(trials);
        [tmp_training,~] = datasample(trials,floor(ntrials*(2/3)),'Replace',false);
        tmp_test = trials(~ismember(trials,tmp_training));
        
        % create these data sets
        data_training = data;
        data_training.trial = data_training.trial(tmp_training);
        data_training.time = data_training.time(tmp_training);
        
        % Average them
        cfg_tl = [];
        % Matrix to estimate noise for eLORETA
        cfg_tl.covariance = 'yes';
        % Time Window to estimate noise
        cfg_tl.covariancewindow = [baseline(1) baseline(2)];
        cfg_tl.removemean = 'no';

        data_tl_training = ft_timelockanalysis(cfg_tl, data_training);

        data_test = data;
        data_test.trial = data_test.trial(tmp_test);
        data_test.time = data_test.time(tmp_test);
        data_tl_test = ft_timelockanalysis(cfg_tl, data_test);
        % compute training sources...
        cfg_sa.(cfg_sa.method).lambda = lambda(i);
        tmp_source_training = ft_sourceanalysis(cfg_sa,data_tl_training);
        % time-resolved power computation
        pow = nan(size(tmp_source_training.pos,1), size(data_tl_training.avg,2));
        for itime=1:size(tmp_source_training.pos,1)
            try
                pow(itime,:) = sum(abs(tmp_source_training.avg.mom{itime}).^2, 1);
            catch
            end
        end
        tmp_source_training.avg.pow = pow;
        % compute test sources...
        tmp_source_test = ft_sourceanalysis(cfg_sa,data_tl_test);
        % time-resolved power computation
        pow = nan(size(tmp_source_test.pos,1), size(data_tl_test.avg,2));
        for itime=1:size(tmp_source_test.pos,1)
            try
                pow(itime,:) = sum(abs(tmp_source_test.avg.mom{itime}).^2, 1);
            catch
            end
        end
        tmp_source_test.avg.pow = pow;

        % ...and their similarity at timepoint time_check_pnt
        corr_mat_tmp = corrcoef(tmp_source_training.avg.pow(:,time_check_pnt),tmp_source_test.avg.pow(:,time_check_pnt),'rows','complete');
        tmp_corr(nfold) = corr_mat_tmp(2);

        
    end
    tmp_corr_mean = mean(tmp_corr);
    myCorr(i) = tmp_corr_mean;
    fprintf('Lambda: %d \n',lambda(i))
    fprintf('Corr: %d \n',myCorr(i))
end

close(f)
% calculate dissimilarity between training and test set at all lambdas
dissimilarity = ones(size(myCorr))-myCorr;
[~,min_lambda] = min(dissimilarity);
fprintf('Minimum Error Lambda: %d\n',lambda(min_lambda))

% find lambda of minimum dissimilarity
% calculate the first derivative (i.e. the change of error)
deriv = diff(dissimilarity);
[~,min_deriv] = min(deriv);
fprintf('Max Loss at lambda: %d\n',lambda(min_deriv))

% Plot dissimilarity at different lambdas an its derivative
figure
plot(1:length(dissimilarity),dissimilarity)
hold on
plot(1:length(deriv),deriv)
legend('dissimilarity','1. derivative')

% base new lambda range between the two lambda where the highest drop in
% error occured.

new_lambda = linspace(lambda(min_deriv-1),lambda(min_deriv+1),10);
for i = 1:numel(new_lambda)
    cfg_sa.(cfg_sa.method).lambda = new_lambda(i);
    tmp_source_training = ft_sourceanalysis(cfg_sa,data_tl_training);
    
    tmp_source_test = ft_sourceanalysis(cfg_sa,data_tl_test);

    tmp_corr = corrcoef(tmp_source_training.avg.pow',tmp_source_test.avg.pow','rows','complete');
    myCorr(i) = tmp_corr(2);
    fprintf('Lambda: %d \n',new_lambda(i))
    fprintf('Corr: %d \n',myCorr(i))
end
% calculate dissimilarity between training and test set at all lambdas
dissimilarity = ones(size(myCorr))-myCorr;
% find lambda of minimum dissimilarity
[~,min_lambda] = min(dissimilarity);
fprintf('Minimum Error Lambda: %d\n',new_lambda(min_lambda))
% calculate the first derivative (i.e. the change of error)
deriv = diff(dissimilarity);
[~,min_deriv] = min(deriv);

% Plot it
figure
plot(1:length(dissimilarity),dissimilarity)
hold on
plot(1:length(deriv),deriv)
legend('dissimilarity','1. derivative')


lambda_gcv = new_lambda(min_deriv);




%% Sources for training and test dataset were calculated. 
% TODO: Next step is to evaluate how different the solutions are for each lambda,
% which shall be implemented by the distance of the inverse Maxima in both
% sets

% find distance between maximia to next lambda step
posmaxdiff = zeros(size(posmax,1)-1,1);
for i = 1:size(posmax,1)-1
    posmaxdiff(i) = sqrt(sum((posmax(i+1,:)-posmax(i,:)).^2));
end

[~,lambda_idx] = min(find(posmaxdiff>1))
cv_lambda = lambda(idx);

figure;
scatter(lambda,posmaxdiff)

end